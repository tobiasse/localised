#!/bin/bash

K=273.15
# Density water at 25℃ in kg/m³
D=997.0479
# Seconds per day
S=86400

# Download listed CORDEX datasets, listing should be a CSV file corresponding to the output of the crawler tool.
# $1 Is a CSV file that contains the download arguments.
# $2 Is the download path.
function download {
  local downloads=${1-}
  local path=${2:-}

  while read -r line; do
    # shellcheck disable=SC2207
    local args=($(tr "," "\n" <<<"$line"))
    # 0 exp, 1 temporal, 2 variable, 3 GCM, 4 RCM, 5 ensemble, 6 start, 7 end
    echo "Downloading... ${args[0]}-${args[1]}-${args[2]}-${args[3]}-${args[4]}-${args[5]}.zip to $path"
    download-cordex -s "${args[0]}" -t "${args[1]}" -v "${args[2]}" -g "${args[3]}" \
      -r "${args[4]}" -e "${args[5]}" --start "${args[6]}" --end "${args[7]}" \
      "${path}${args[0]}-${args[1]}-${args[2]}-${args[3]}-${args[4]}-${args[5]}-${args[6]}-${args[7]}.zip"
  done <"$downloads"
}

# Merge a CORDEX dataset zip archive to a NetCDF, all files within the archive are merge by applying the cdo mergetime
# operator. File is created alongside the zip archive by using the archive name as its filename.
# $1 Is the CORDEX zip archive.
function merge {
  local zip=${1-}

  local dir
  local name
  dir=$(dirname "$zip")
  name=$(basename "$zip")

  local args=()
  while read -r -a line; do
    # shellcheck disable=SC2207
    local f=($(tr "." "\n" <<<"${line[3]}"))
    if [[ "${#f[@]}" == 0 ]]; then continue; fi
    if [[ "${f[-1]}" == "nc" ]]; then
      args+=("${dir}/${line[3]}")
    fi
  done < <(unzip -l "$zip")

  echo "Extracting... $zip to $dir"
  unzip -d "$dir" -j "$zip" >/dev/null

  echo "Merging..." "${args[@]}" "to ${dir}/${name%.*}.nc"
  cdo mergetime "${args[@]}" "${dir}/${name%.*}.nc" >/dev/null 2>&1

  rm "${args[@]}"

  echo "${dir}/${name%.*}.nc"
}

# Remap rotated latlon grid to a latlon grid by applying bilinear interpolation.
# $1 Is a NetCDF file using a rotated latlon grid.
# $2 Is a CDO grid definition used for the remapping.
# $3 Is the output path, please note if no path is given the output file is created with the same name as the input file
#    at script execution location.
function remap {
  local grid=${1-}
  local in=${2-}
  local out=${3-}

  echo "Remapping... $in to $out"
  cdo -invertlat -remapbil,"$grid" "$in" "$out" >/dev/null 2>&1
}

# Calculates annual mean precipitation (mPr) in millimeter per day (mm/day) at 25℃.
# $1 Input: filepath of daily precipitation flux data in kilogramm per cubic meter and second (kg/sm³)
# $2 Output: filepath of annual mean precipitation in millimeter per day (mm/day) at 25℃
function mPr {
  local in=${1-}
  local out=${2-}

  # -mulc,1000 := precipitation in m/day to mm/day
  # -mulc,"$S" := precipitation in m/s to m/day
  # -divc,"$D" := precipitation flux kg/sm² to precipitation in m/s
  echo "mPr... $in to $out"
  cdo \
    --timestat_date first \
    -mulc,1000 \
    -mulc,"$S" \
    -divc,"$D" \
    -yearmean \
    "$in" "$out" >/dev/null 2>&1
}

# Calculates seasonal mean precipitation (mPr) in millimeter per day (mm/day) at 25℃.
# $1 Input: filepath of daily precipitation flux data in kilogramm per cubic meter and second (kg/sm³)
# $2 Output: filepath of seasonal mean precipitation in millimeter per day (mm/day) at 25℃
function seasmPr {
  local in=${1-}
  local out=${2-}

  # -mulc,1000 := precipitation in m/day to mm/day
  # -mulc,"$S" := precipitation in m/s to m/day
  # -divc,"$D" := precipitation flux kg/sm² to precipitation in m/s
  echo "seasmPr... $in to $out"
  cdo \
    --timestat_date first \
    -mulc,1000 \
    -mulc,"$S" \
    -divc,"$D" \
    -seasmean \
    "$in" "$out" >/dev/null 2>&1
}

# Calculates annual mean total precipitation (tPr) in millimeter (mm).
# $2 Input: filepath of daily precipitation flux data in kilogramm per cubic meter and second (kg/sm³)
# $3 Output: filepath of annual mean total precipitation (mm)
function tPr {
  local in=${1-}
  local out=${2-}

  echo "tPr... $in to $out"
  cdo \
    --timestat_date first \
    -yearsum \
    -mulc,1000 \
    -mulc,"$S" \
    -divc,"$D" \
    "$in" "$out" >/dev/null 2>&1
}

# Calculates seasonal mean total precipitation (tPr) in millimeter (mm).
# $2 Input: filepath of daily precipitation flux data in kilogramm per cubic meter and second (kg/sm³)
# $3 Output: filepath of seasonal mean total precipitation (mm)
function seastPr {
  local in=${1-}
  local out=${2-}

  echo "seastPr... $in to $out"
  cdo \
    --timestat_date first \
    -seassum \
    -mulc,1000 \
    -mulc,"$S" \
    -divc,"$D" \
    "$in" "$out" >/dev/null 2>&1
}

# Calculates annual mean temperature (mTp) in Celsius (℃).
# $1 Input: filepath of daily temperature data in Kelvin (K)
# $2 Output: filepath of annual mean temperature data in Celsius (℃)
function mTp {
  local in=${1-}
  local out=${2-}

  echo "mTp... $in to $out"
  cdo \
    --timestat_date first \
    -subc,"$K" \
    -yearmean \
    "$in" "$out" >/dev/null 2>&1
}

# Calculates seasonal mean temperature (mTp) in Celsius (℃).
# $1 Input: filepath of daily temperature data in Kelvin (K)
# $2 Output: filepath of seasonal mean temperature data in Celsius (℃)
function seasmTp {
  local in=${1-}
  local out=${2-}

  echo "seasmTp... $in to $out"
  cdo \
    --timestat_date first \
    -subc,"$K" \
    -seasmean \
    "$in" "$out" >/dev/null 2>&1
}

# Calculate annual Heating Degree Days (HDD)
# $1 The base temperature in Celsius
# $2 Input: filepath of daily temperature data in Kelvin (K)
# $3 Output: filepath of HDD
function hdd {
  local threshold=${1-}
  local in=${2-}
  local out=${3-}

  # HDD(t, x) = threshold - file(t, x) if file(t, x) < threshold
  # -yearsum := Calculate yearly sum of HDD
  # -addc,"$threshold" -mulc,-1 := threshold - file(t, x) = threshold + (-1 * file(t, x))
  # -setvrange,"$(bc <<<"-1 * $K"),$threshold" := if file(t, x) < threshold; set each value smaller than absolute zero
  # OR greater than threshold to missing
  # -subc,"$K" := Convert Kelvin to Celsius
  echo "HDD... $in to $out"
  cdo \
    --timestat_date first \
    -yearsum \
    -addc,"$threshold" -mulc,-1 \
    -setvrange,"$(bc <<<"-1 * $K"),$threshold" \
    -subc,"$K" \
    "$in" "$out" >/dev/null 2>&1
}

# Calculate Cooling Degree Days (HDD)
# $1 The base temperature in Celsius
# $3 Input: filepath of daily temperature data in Kelvin (K)
# $4 Output: filepath of CDD
function cdd {
  local threshold=${1-}
  local in=${2-}
  local out=${3-}

  # CDD(t, x) = file(t, x) - threshold if file(t, x) > threshold
  # -yearsum := Calculate yearly sum of HDD
  # -subc,"$threshold" := file(t, x) - threshold
  # -setrtomiss,"$(bc <<<"-1 * $K"),$threshold" := if file(t, x) > threshold; set each value smaller than threshold
  # AND greater than absolute zero to missing
  # -subc,"$K" := Convert Kelvin to Celsius
  echo "CDD... $in to $out"
  cdo \
    --timestat_date first \
    -yearsum \
    -subc,"$threshold" \
    -setrtomiss,"$(bc <<<"-1 * $K"),$threshold" \
    -subc,"$K" \
    "$in" "$out" >/dev/null 2>&1
}

download "data/raw/downloads.csv" "data/raw/"

python="
from re import compile
from os import listdir
from os import rename
from os.path import join

path = 'data/raw/'
regex = compile(r'(.*?)(\d{4}(?:;\d{4})+)-(\d{4}(?:;\d{4})+)(.zip)')

for f in listdir(path):
  if m := regex.match(f):
    start = m.group(2).split(';')[0]
    end = m.group(3).split(';')[-1]
    rename(join(path, f), join(path, '{}{}-{}{}'.format(m.group(1), start, end, m.group(4))))
"

python <<<"$python"

for f in data/raw/*.zip; do
  merge "$f"
done

rm data/raw/*.zip

while read -r exp; do
  if [[ $exp == "value" ]]; then continue; fi
  while read -r var; do
    if [[ $var == "value" ]]; then continue; fi
    while read -r gcm; do
      if [[ $gcm == "value" ]]; then continue; fi
      while read -r rcm; do
        if [[ $rcm == "value" ]]; then continue; fi
        if [[ -n $(
          shopt -s nullglob
          echo data/raw/"$exp"*"$var"*"$gcm"*"$rcm"*.nc
        ) ]]; then
          cdo mergetime data/raw/"$exp"*"$var"*"$gcm"*"$rcm"*.nc "data/raw/$exp-$var-$gcm-$rcm.nc"
        fi
      done < <(xsv frequency -s "rcm" data/raw/downloads.csv | xsv select "value")
    done < <(xsv frequency -s "gcm" data/raw/downloads.csv | xsv select "value")
  done < <(xsv frequency -s "variable" data/raw/downloads.csv | xsv select "value")
done < <(xsv frequency -s "experiment" data/raw/downloads.csv | xsv select "value")

rm data/raw/*daily_mean*.nc

path="data/interim/"
for f in data/raw/*.nc; do
  file=$(basename "$f" ".nc")
  if [[ "$f" == *"precipitation"* ]]; then
    mPr "$f" "$path/$file-annual_mPr.nc"
    seasmPr "$f" "$path/$file-seasonal_mPr.nc"
    tPr "$f" "$path/$file-annual_tPr.nc"
    seastPr "$f" "$path/$file-seasonal_tPr.nc"
  elif [[ "$f" == *"temperature"* ]]; then
    mTp "$f" "$path/$file-annual_mTp.nc"
    seasmTp "$f" "$path/$file-seasonal_mTp.nc"
    hdd 18 "$f" "$path/$file-hdd.nc"
    cdd 21 "$f" "$path/$file-cdd.nc"
  fi
done

for exp in "historical" "rcp_2_6" "rcp_4_5" "rcp_8_5"; do
  for pvar in "annual_mPr" "seasonal_mPr" "annual_tPr" "seasonal_tPr"; do
    cdo ensmean data/interim/"$exp"*"mean_precipitation_flux"*"$pvar".nc "data/proc/$exp-$pvar.nc"
  done
  for tvar1 in "annual_mTp" "seasonal_mTp" "hdd" "cdd"; do
    for tvar2 in "2m_air_temperature" "minimum_2m_temperature_in_the_last_24_hours" "maximum_2m_temperature_in_the_last_24_hours"; do
      cdo ensmean data/interim/"$exp"*"$tvar2"*"$tvar1".nc "data/proc/$exp-${tvar2}_${tvar1}.nc"
    done
  done
done

for f in data/proc/*.nc; do
  var=$(cdo --silent showname "$f" | xargs)
  cdo -invertlat -remapbil,grid "$f" "data/proc/$(basename "$f" ".nc")-remapped.nc"
  rescale -a "me" -v "$var" --all_touched \
    "data/proc/$(basename "$f" ".nc")-remapped.nc" data/raw/nuts3.shp "data/proc/$(basename "$f" ".nc")-nuts3.geojson"
  rm "data/proc/$(basename "$f" ".nc")-remapped.nc"
done

for f in data/proc/*.geojson; do
  python="
from re import compile
from geopandas import read_file
gdf = read_file('$f')
regex = compile(r'.*_(\d{4})-\d{2}-(\d{2})_\d{2}:\d{2}')
cols = list()
for c in gdf.columns:
  match = regex.match(c)
  if not match:
    cols.append(c)
    continue
  year, month = match.group(1), int(match.group(2))
  if month == 3:
    cols.append('{}_spring'.format(year))
  elif month == 6:
    cols.append('{}_summer'.format(year))
  elif month == 9:
    cols.append('{}_autumn'.format(year))
  elif month == 12:
    cols.append('{}_winter'.format(year))
  else:
    cols.append('{}'.format(year))
gdf.columns = cols
gdf.to_file('$f', driver='GeoJSON')
  "
  python <<<"$python"
done

for f in data/proc/*{annual,cdd,hdd}*.geojson; do
  python="
from re import compile
from geopandas import read_file
from pandas import DataFrame, read_csv, concat

try:
  df = read_csv('data/proc/data.csv')
except:
  df = DataFrame()

gdf = read_file('$f')
regex = compile(r'\d{4}(_\w+)?')
allowed = ['CNTR_CODE', 'NUTS_ID', 'NAME_LATN', 'NUTS_NAME']
labels = [c for c in gdf.columns if not (regex.match(c) or c in allowed)]
gdf.drop(labels, axis=1, inplace=True)
exp, metric, *_ = '$(basename "$f")'.split('-')
gdf['EXP'] = exp
gdf['METRIC'] = metric
df = concat([df, gdf], ignore_index=True)
df.sort_index(axis=1, inplace=True)
df.to_csv('data/proc/data.csv', index=False)
  "
  python <<<"$python"
done
