from __future__ import annotations

from typing import List

from pandas import Series


def check(
        row: Series,
        columns: List[int | str],
        classes: List[int | str],
        default_class: int | str,
        column_name: str
) -> Series:
    cls: int | str = default_class
    counts: List[int] = [0] * len(classes)

    for value in row[columns]:
        counts[classes.index(value)] += 1

    for i, count in enumerate(counts):
        if count > len(columns) / 2:
            cls = classes[i]
            break

    row[column_name] = cls
    return row
