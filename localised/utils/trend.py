from __future__ import annotations

from typing import Dict
from typing import List
from typing import NamedTuple

from geopandas import GeoSeries
from numpy import all
from numpy import isnan
from numpy import nan
from numpy.typing import NDArray
from pandas import Series
from pyhomogeneity import pettitt_test
from pymannkendall import original_test

CLASSES: Dict[str, str] = {
    'increasing': 'Increase',
    'decreasing': 'Decrease',
    'no trend': 'No Change',
}

NOT_KNOWN = 'Not Known'

Result = NamedTuple('Result', trend=str, p=float, slope=float)
PettittResult = NamedTuple('PettittResult', cp=int, p=float)


def test_trend(row: GeoSeries | Series, columns: List[int], alpha: float = 0.05) -> GeoSeries | Series:
    vals: NDArray[float] = row.values[columns].astype(float)

    if all(isnan(vals)):
        row['trend'] = NOT_KNOWN
        row['slope'] = nan
        row['p'] = nan

    else:
        try:
            result: Result = original_test(vals, alpha=alpha)
            row['trend'] = CLASSES.get(result.trend, NOT_KNOWN)
            row['slope'] = result.slope
            row['p'] = result.p

        except Exception:
            row['trend'] = NOT_KNOWN
            row['slope'] = nan
            row['p'] = nan

    return row


def test_change_point(
        row: GeoSeries | Series,
        columns: List[int],
        alpha: float = 0.05,
        sim: int = 0
) -> GeoSeries | Series:
    vals: NDArray[float] = row.values[columns].astype(float)
    idx: NDArray[str] = row.index[columns]

    if all(isnan(vals)):
        row['cp'] = ''
        row['cpp'] = nan

    else:
        try:
            result: PettittResult = pettitt_test(vals, alpha=alpha, sim=sim)
            row['cp'] = idx[result.cp]
            row['cpp'] = result.p

        except Exception:
            row['cp'] = ''
            row['cpp'] = nan

    return row
