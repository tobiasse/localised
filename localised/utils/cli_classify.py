from __future__ import annotations

from sys import stdout
from typing import Tuple

from click import File
from click import Path
from click import argument
from click import command
from click import option
from geopandas import GeoDataFrame
from geopandas import read_file
from numpy import empty
from numpy import isnan
from numpy import logical_and
from numpy.typing import NDArray
from pandas import DataFrame

from localised.utils.parse import frame


@command()
@option(
    '-c',
    '--col',
    type=str,
    default='t2m',
    is_eager=True,
    help=''
)
@option(
    '--classes',
    type=str,
    multiple=True,
    default=('Low', 'Moderate', 'High'),
    help=''
)
@option(
    '--limits',
    type=float,
    nargs=2,
    multiple=True,
    default=((0, .005), (.005, .05), (.05, 1.0)),
    help=''
)
@option(
    '--nan_class',
    type=str,
    default='Not Known',
    help=''
)
@argument(
    'data',
    type=File(),
    callback=lambda ctx, para, value: frame(
        paras=['col'],
    )(ctx, para, value) if value.name.endswith('.csv') else frame(
        paras=['col'],
        reader=read_file
    )(ctx, para, value)
)
@argument(
    'out',
    type=Path(dir_okay=False, writable=True),
    required=False,
)
def classify(
        col: str,
        limits: Tuple[Tuple[float, float], ...],
        classes: Tuple[str, ...],
        nan_class: str,
        data: DataFrame | GeoDataFrame,
        out: str | None,
) -> None:
    values: NDArray[float] = data[col].values

    classified: NDArray[object] = empty(values.size, dtype=object)
    classified[isnan(values)] = nan_class

    for i, (upper, lower) in enumerate(limits):
        classified[logical_and(values >= upper, values < lower)] = classes[i]

    data[f'{col}_class'] = classified

    if out and isinstance(data, GeoDataFrame):
        data.to_file(out, index=False)
    elif out and isinstance(data, DataFrame):
        data.to_csv(out, index=False)
    else:
        data.to_csv(stdout, index=False)
