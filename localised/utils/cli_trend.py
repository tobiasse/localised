from __future__ import annotations

from sys import stdout
from typing import List

from click import File
from click import Path
from click import argument
from click import command
from click import option
from geopandas import GeoDataFrame
from geopandas import read_file
from pandas import DataFrame

from localised.utils.parse import frame
from localised.utils.parse import parse_ranges
from localised.utils.trend import test_change_point
from localised.utils.trend import test_trend


@command()
@option(
    '-c',
    '--cols',
    type=str,
    callback=parse_ranges(formatting='{}'),
    help=''
)
@option(
    '--pettitt',
    type=bool,
    is_flag=True,
    help='Use Pettitt\'s test for change point detection.'
)
@option(
    '--alpha',
    type=float,
    default=0.05,
    help='Significance level for trend detection.'
)
@option(
    '--sim',
    type=int,
    default=0,
    help='Number of simulations for Pettitt\'s test.'
)
@argument(
    'data',
    type=File(),
    callback=lambda ctx, para, value: frame(
    )(ctx, para, value) if value.name.endswith('.csv') else frame(
        reader=read_file
    )(ctx, para, value)
)
@argument(
    'out',
    type=Path(dir_okay=False, writable=True),
    required=False,
)
def trend(
        cols: List[str],
        pettitt: bool,
        alpha: float,
        sim: int,
        data: DataFrame | GeoDataFrame,
        out: str | None,
) -> None:
    result: DataFrame | GeoDataFrame = data.apply(test_trend, axis=1, args=(list(map(int, cols)),), alpha=alpha)

    if pettitt:
        result = result.apply(test_change_point, axis=1, args=(list(map(int, cols)),), alpha=alpha, sim=sim)

    if out and isinstance(data, GeoDataFrame):
        result.to_file(out, index=False)
    elif out and isinstance(data, DataFrame):
        result.to_csv(out, index=False)
    else:
        result.to_csv(stdout, index=False)
