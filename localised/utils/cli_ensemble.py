from __future__ import annotations

from sys import stdout
from typing import Tuple

from click import File
from click import Path
from click import argument
from click import command
from click import option
from geopandas import GeoDataFrame
from geopandas import read_file
from numpy import unique
from numpy.typing import NDArray
from pandas import DataFrame

from localised.utils.ensemble import check
from localised.utils.parse import frame


@command()
@option(
    '-c',
    '--columns',
    type=str,
    multiple=True,
    is_eager=True,
    help=''
)
@option(
    '--default',
    type=str,
    default='Not Known',
    help=''
)
@option(
    '--name',
    type=str,
    default='Class',
    help=''
)
@argument(
    'data',
    type=File(),
    callback=lambda ctx, para, value: frame(
        paras=['columns'],
        paras_extractor=lambda v: list(v),
    )(ctx, para, value) if value.name.endswith('.csv') else frame(
        paras=['columns'],
        paras_extractor=lambda v: list(v),
        reader=read_file
    )(ctx, para, value)
)
@argument(
    'out',
    type=Path(dir_okay=False, writable=True),
    required=False,
)
def ensemble(
        columns: Tuple[str, ...],
        default: str,
        name: str,
        data: DataFrame | GeoDataFrame,
        out: str | None,
) -> None:
    classes: NDArray[str] = unique(data[list(columns)].values)
    result: DataFrame | GeoDataFrame = data.apply(check, axis=1, args=(list(columns), list(classes), default, name))

    if out and isinstance(data, GeoDataFrame):
        result.to_file(out, index=False)
    elif out and isinstance(data, DataFrame):
        result.to_csv(out, index=False)
    else:
        result.to_csv(stdout, index=False)
