from __future__ import annotations

from typing import List
from typing import Tuple

from click import File
from click import Path
from click import argument
from click import command
from click import option
from pandas import DataFrame
from pandas import concat

from localised.utils.parse import frame


@command()
@option(
    '-o',
    '--on',
    type=str,
    is_eager=True,
    help='The column to merge on.',
)
@option(
    '-c',
    '--columns',
    type=str,
    multiple=True,
    is_eager=True,
    help='The columns to merge.',
)
@option(
    '-f',
    '--files',
    type=File(),
    multiple=True,
    callback=lambda ctx, para, value: [
        frame(paras=['columns'], paras_extractor=lambda v: list(v))(ctx, para, v)
        for v in value
    ],
)
@option(
    '-s',
    '--suffixes',
    type=str,
    multiple=True,
)
@argument(
    'out',
    type=Path(dir_okay=False, writable=True),
    required=False,
)
def merge(
        on: str,
        columns: Tuple[str, ...],
        files: List[DataFrame, ...],
        suffixes: Tuple[str, ...],
        out: str | None,
) -> None:
    to_concat: List[DataFrame] = list()
    for i, f in enumerate(files):
        df = f[list(columns)].copy()
        df.columns = [f'{c}{suffixes[i]}' for c in columns]
        df[on] = f[on]
        to_concat.append(df)

    result = concat(to_concat, axis=1, keys=on)
    result.columns = result.columns.droplevel(0)
    result = result.loc[:, ~result.columns.duplicated()].copy()
    result.to_csv(out, index=False)
