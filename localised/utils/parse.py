from __future__ import annotations

import re
from typing import Any
from typing import Callable
from typing import List
from typing import Set
from typing import Tuple

from click import BadParameter
from click import Context
from click import Parameter
from click import UsageError
from geopandas import GeoDataFrame
from pandas import DataFrame
from pandas import read_csv
from pandas.io.parsers import TextFileReader


def parse_ranges(sep: str = ',', formatting: str = '{:0>2}') -> Callable[[Context, Parameter, str], List[str]]:
    """Parses a string of numerical ranges to a list of numerical values by applying the provided
    formatting pattern. By keeping the default values the string "1-3,4,7,8" is parsed to 01,02,03,04,07,08.
    Grammar is:

    range_string = number | range {sep, number | range};
    range = number, range_sep, number;

    number = digit, {digit};
    digit = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9";

    sep = ","; (* can be altered *)
    range_sep = "-";

    Args:
        sep: The item separator.
        formatting: Formatting pattern (should use Pythons formatting mini language) to apply to the parsed values.

    Returns:
        The parsed values a list of strings.
    """

    def wrapped(ctx: Context, para: Parameter, value: str) -> List[str]:
        sink = list()
        for c in value.split(sep):
            c = c.strip()
            if re.match(r'\d+ ?- ?\d+', c):
                s, e = c.split('-')
                c = list(map(formatting.format, map(str, range(int(s), int(e) + 1))))
            elif re.match(r'\d+', c):
                c = [formatting.format(c)]
            else:
                raise BadParameter(f'Can not parse {c}', ctx=ctx, param=para)
            sink += c
        return sink

    return wrapped


def frame(
        paras: List[str] = (),
        paras_extractor: Callable[[Any], List[str]] | None = None,
        labels: List[str] = (),
        reader=read_csv,
        **kwargs
) -> Callable[[Context, Parameter, Tuple[str, ...] | str | None], DataFrame | GeoDataFrame | TextFileReader]:
    """Checks availability of column labels

    Function intended to be used as callback (validator) of click parameters. It returns a function that expects a file
    type as argument. The file type is read/opened by the provided pandas reader function. To extract column labels
    from other click parameters (user input for options), these parameters must be set to eager (`is_eager=True`) to
    allow the function to find them in the provided click parameter context. Per default only strings are accepted as
    parameter values. In case of other types, a custom extractor function must be provided via the paras_extractor
    attribute.

    Args:
        paras: List of context parameter keys where to fetch from column labels.
        paras_extractor: Function to extract column labels from parameter values which are not strings.
        labels: List of column labels to validate.
        reader: A pandas data reader function, e.g. read_csv, read_json, etc.
        kwargs: Extra keyword arguments are delegated to the reader function.

    Returns:
        Validator function
    """

    def wrapped(
            ctx: Context,
            param: Parameter,
            val: Tuple[str, ...] | str | None
    ) -> Tuple[GeoDataFrame | DataFrame | TextFileReader, ...] | GeoDataFrame | DataFrame | TextFileReader | None:
        if not val:  # Handle called with val=None if `required=False`
            return val

        cols: List[str] = list(labels)

        for p in paras:
            label: str | Any = ctx.params.get(p, None)
            if not label:
                raise UsageError(f'Unknown parameter {p}', ctx=ctx)
            if isinstance(label, str):
                cols.append(label)
            elif paras_extractor:
                cols += paras_extractor(label)
            else:
                raise UsageError(f'Unknown value type assigned to {p}', ctx=ctx)

        if isinstance(val, tuple):
            dfs: Tuple[GeoDataFrame | DataFrame | TextFileReader, ...] = tuple(reader(v, **kwargs) for v in val)
            buffs: Tuple[str, ...] = val
        else:
            dfs: Tuple[GeoDataFrame | DataFrame | TextFileReader, ...] = (reader(val, **kwargs),)
            buffs: Tuple[str, ...] = (val,)

        for buf, df in zip(buffs, dfs):
            if isinstance(df, TextFileReader):
                df_cols: Set[str] = set(df.read(nrows=0).columns)
            else:
                df_cols: Set[str] = set(df.columns)

            unique_cols: Set[str] = set(cols)
            intersection: Set[str] = unique_cols & df_cols

            if len(intersection) != len(unique_cols):
                missing: Set[str] = intersection ^ unique_cols
                # noinspection PyUnresolvedReferences
                raise BadParameter(f'Missing {missing} in {buf.name}', ctx=ctx, param=param)

        return dfs[-1] if len(dfs) == 1 else dfs

    return wrapped
