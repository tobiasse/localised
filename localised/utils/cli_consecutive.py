from __future__ import annotations

from typing import Callable
from typing import Tuple

from click import Choice
from click import Path
from click import argument
from click import command
from click import option
from netCDF4 import Dataset
from netCDF4 import Variable
from numpy.typing import NDArray

from localised.utils.consecutive import cc
from localised.utils.consecutive import condition
from localised.utils.consecutive import cumulative
from localised.utils.consecutive import disaggregated
from localised.utils.consecutive import nc_copy
from localised.utils.consecutive import parse_condition

MODES = {
    'cum': cumulative,
    'disagg': disaggregated
}


@command()
@option(
    '-v',
    '--var',
    type=str,
    required=True,
    help='Counting number of consecutive events for the selected variables, e.g., "var1,var2,...".'
)
@option(
    '-s',
    '--start_condition',
    type=str,
    default='== 1',
    show_default=True,
    callback=parse_condition,
    help='The counting start condition, e.g., OPERATION (<, >, ==, !=, <=, >=) NUMBER.'
)
@option(
    '-e',
    '--end_condition',
    type=str,
    default='!= 1',
    show_default=True,
    callback=parse_condition,
    help='The counting end condition, e.g., OPERATION (<, >, ==, !=, <=, >=) NUMBER.'
)
@option(
    '-m',
    '--mode',
    type=Choice(list(MODES.keys())),
    default='cum',
    show_default=True,
    callback=lambda ctx, param, value: (value, MODES[value]),
    help='The counting mode.'
)
@option(
    '--mn',
    type=int,
    default=3,
    show_default=True,
    help='The minimum number of consecutive events.'
)
@option(
    '--set_to',
    type=int,
    default=1,
    show_default=True,
    help='Set consecutive count to this value, only applied if counting mode is disagg.'
)
@argument(
    'in_nc',
    type=Path(exists=True, dir_okay=False),
    required=True,
)
@argument(
    'out_nc',
    type=Path(dir_okay=False, writable=True),
    required=True,
)
def consecutive(
        var: str,
        start_condition: Tuple[str, float],
        end_condition: Tuple[str, float],
        mode: Tuple[str, Callable[[NDArray[int | float], int], NDArray[int | float]]],
        mn: int,
        set_to: int,
        in_nc: str,
        out_nc: str
) -> None:
    """Counts for the selected variables in a NetCDF the number of consecutive events.

    Count for one or more selected variables (-v, --var) the number of events where the give start condition (-s,
    --start_condition) is fulfilled till the end condition (-e, --end_condition) at least for the minimum (--mn) number
    of steps. The mode (-m, --mode) determines if the number of consecutive events is stored cumulative or disaggregated
    , see examples. The number of consecutive events is stored in the given NETCDF by adding the suffix consecutive to
    the selected variables.

    Examples:

    consecutive -v exceedance -s "==1" -e "!=1" -m cum --mn 3 foo.nc

    0 1 1 0 1 1 1 1 0 0 0 1 0 1 1 1 < input

    0 0 0 0 0 0 0 4 0 0 0 0 0 0 0 3 < output

    consecutive -v exceedance -s "==1" -e "!=1" -m disagg --mn 3 foo.nc

    0 1 1 0 1 1 1 1 0 0 0 1 0 1 1 1 < input

    0 0 0 0 1 1 1 1 0 0 0 0 0 1 1 1 < output
    """
    nc: Dataset = Dataset(in_nc)
    sink: Dataset = Dataset(out_nc, mode='w')

    sc = condition(*start_condition)
    ec = condition(*end_condition)

    method, func = mode

    nc_copy(nc, sink, var)

    src_var: Variable = nc.variables[var]
    dst_var: Variable = sink.variables[var]

    cc(src_var, dst_var, start_condition=sc, end_condition=ec, mode=func, mn=mn, set_to=set_to)

    nc.close()
    sink.close()

    # with Dataset(netcdf, mode='a') as n:
    #     var = var.split(',')
    #     for v in var:
    #         assert v in n.variables
    #         assert f'{v}_consecutive' not in n.variables
    #
    #         src = n.variables[v]
    #
    #         dst = n.createVariable(f'{v}_consecutive', 'i2', src.dimensions, fill_value=0)
    #         dst.units = get_attr('units', src, '')
    #         dst.long_name = get_attr('long_name', src, '')
    #         dst.standard_name = get_attr('standard_name', src, '')
    #         dst.start_condition = start_condition
    #         dst.end_condition = end_condition
    #         dst.mode = mode
    #         dst.mn = mn
    #
    #         cc(src, dst, start_condition=sc, end_condition=ec, mode=m, mn=mn, set_to=set_to)
