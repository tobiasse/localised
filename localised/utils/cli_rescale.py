import geopandas as gpd
from click import Choice
from click import Path
from click import argument
from click import command
from click import option
from click import progressbar
from netCDF4 import Dataset
from netCDF4 import num2date
from numpy import NaN
from numpy.ma import masked

from localised.utils.rescale import Masking
from localised.utils.rescale import md
from localised.utils.rescale import me
from localised.utils.rescale import mn
from localised.utils.rescale import mx
from localised.utils.rescale import output_factory
from localised.utils.rescale import sm

LAT = 'latitude'
LON = 'longitude'
TIME = 'time'

_aggregations = {
    'min': mn,
    'max': mx,
    'mean': me,
    'median': md,
    'sum': sm,
}


@command()
@option(
    '-a',
    '--agg',
    type=Choice(list(_aggregations.keys())),
    default='mean',
    show_default=True,
    help='The rescaling operation'
)
@option(
    '-v',
    '--var',
    required=True,
    type=str,
    help='The target variable'
)
@option(
    '--all_touched',
    is_flag=True,
    help='If True, all cells touched by geometries will be burned in'
)
@option(
    '--time_var',
    type=str,
    default=TIME,
    show_default=True,
    help='Name of the time variable'
)
@option(
    '--lat_var',
    type=str,
    default=LAT,
    show_default=True,
    help='Name of the latitude variable'
)
@option(
    '--lon_var',
    type=str,
    default=LON,
    show_default=True,
    help='Name of the longitude variable'
)
@argument(
    'netcdf',
    required=True,
    type=Path(exists=True, dir_okay=False)
)
@argument(
    'mask',
    required=True,
    type=Path(exists=True, dir_okay=False)
)
@argument(
    'rescaled',
    required=True,
    type=Path(dir_okay=False, writable=True)
)
def rescale(
        agg: str,
        var: str,
        all_touched: bool,
        time_var: str,
        lat_var: str,
        lon_var: str,
        netcdf: str,
        mask: str,
        rescaled: str
) -> None:
    """Rescales the cell values of netCDF target variable for all time steps by applying the selected aggregation.
    Rescaling units are the geometries provided by a vector file like a shapefile or geojson.

    Please note, uses the statistical mean when mean rescaling is selected. Therefore, you should not use this
    tool for rescaling large spatial units!
    """
    gdf = gpd.read_file(mask)
    rescaling = _aggregations[agg]
    with Dataset(netcdf) as src:
        ext = rescaled.split('.')[-1]
        times = num2date(
            src.variables[time_var][:],
            units=src.variables[time_var].units,
            calendar=src.variables[time_var].calendar
        )

        output = output_factory(ext, table=gdf, variable=var, times=times)
        masker = Masking(src.variables[lat_var][:], src.variables[lon_var][:], all_touched=all_touched)

        with progressbar(list(gdf.iterrows()), label='Rescaling...') as bar:
            for i, series in bar:
                ma = masker.get_mask(series.geometry)
                row = list()
                for data in src.variables[var][:, :, :]:
                    data[ma] = masked
                    if data.mask.all():
                        row.append(NaN)
                    else:
                        row.append(rescaling(data))
                output.write(i, row)

        output.save(rescaled)
