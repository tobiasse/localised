from pathlib import Path
from typing import List
from typing import Tuple
from typing import Union

import click
import geopandas as gpd

from localised.thresher.netcdf import Netcdf
from localised.thresher.threshold import create
from localised.utils.parse import parse_ranges


@click.command()
@click.option('-p', '--percentiles', type=str, required=True, help='Percentile definitions')
@click.option('-y', '--years', type=int, nargs=2, required=True, help='Start and end year of the base period')
@click.option('-m', '--months', type=str, default='1-12', show_default=True, callback=parse_ranges(),
              help='Months to select within base period')
@click.option('-w', '--window', type=int, default=5, show_default=True, help='Size of the subsampling window')
@click.option('--bootstrapping', is_flag=True, help='Turn bootstrapping on')
@click.option('--repetitions', type=int, default=1, show_default=True,
              help='The number of repetitions per base year, if bootstrapping is selected.')
@click.option('--order', type=int, default=0, show_default=True,
              help='Order of year removal per bootstrapping run. Please select either 0 or -1.')
@click.option('--seed', type=int, default=None, help='Define a seed for the random number generator.')
@click.option('-r', '--roi', type=str, default=None, help='Geometric mask')
@click.argument('netcdf', required=True, type=click.Path(exists=True))
@click.argument('threshold', required=True, type=click.Path())
def thresher(
        percentiles: str,
        years: Tuple[int, int],
        months: List[str],
        window: int,
        bootstrapping: bool,
        repetitions: int,
        order: int,
        seed: Union[int, None],
        roi: Union[str, None],
        netcdf: str,
        threshold: str
) -> None:
    """Thresholds generate percentile-based daily thresholds (THRESHOLD) for the analysis of weather extremes.

    This tool implements the methodology described by Zhang et al. (2005) in: "Avoiding inhomogeneity in
    percentile-based indices of temperature extremes." Please, provide a source data NetCDF (NETCDF) and a base period
    definition (-y --years, -m --months, and -w --window). A source NetCDF must have a time, latitude, and longitude
    dimension. Further, you should ensure that the time dimension completely covers your base period selection, e.g., no
    missing years, months, or days. For the base period, you may select single or multiple months (-m --months) by using
    the following syntax: 5 = only May set, 1-5 = selects from January till May, or 1-3,6,9-11 = selects from January
    till March, June, and September till November. The window (-w --window) is always centered around the date of
    threshold calculation and should be an odd integer, and the base year (-y --years) selects all years from the start
    till the end. In one run, you may compute one or multiple thresholds by using the following quantile
    (-p --percentiles) definition syntax: tmp>90 = threshold is the 90th percentile of the source data NetCDF variable
    tmp, or tmp>90,pre<10 = the first threshold is the 90th = percentile of the tmp variable and the second threshold
    is the 10th percentile of the pre variable. Supports the following comparison operators for percentile definitions:
    <, >, <=, >=. The bootstrapping (--bootstrapping) flag turns on the inhomogeneity correction. You may select the
    number of repetitions (--repetitions) per bootstrapping year and the order of year removal (--order) either 0 for
    left to right or -1 for right to left. Additionally you may set a seed (--seed) for the random number generator that
    replaces removed years with a random year from the remaining base years. Further, you may set a region of interest
    (-r --roi), limiting the calculation of thresholds and exceedance to all data within this region. The roi must be a
    vector file (e.g., ESRI shapefile, GeoJSON, etc.), while the algorithm uses only the first geometric object of the
    vector file.

    Example:

    thresholds -p "temp>90" -y 1971 2000 -m "6,7,8" foo.nc thresholds.nc
    """
    fp = Path(threshold)
    nc = Netcdf(Path(netcdf))

    if roi:
        roi = gpd.read_file(roi).geometry[0]

    create(
        fp,
        nc.path,
        percentiles=percentiles.split(','),
        years=years,
        months=list(map(int, months)),
        window=window,
        bootstrapped=bootstrapping,
        repetitions=repetitions,
        order=order,
        seed=seed,
        roi=roi
    ).run()
