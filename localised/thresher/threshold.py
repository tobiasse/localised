import re
from calendar import monthrange
from copy import copy
from datetime import timedelta
from functools import cached_property
from math import ceil
from numbers import Number
from pathlib import Path
from typing import Dict
from typing import List
from typing import Tuple
from typing import Union

from cftime import DatetimeNoLeap
from cftime import datetime
from click import progressbar
from netCDF4 import Dataset
from netCDF4 import date2num
from numpy import float64
from numpy import int32
from numpy import isin
# noinspection PyPep8Naming
from numpy import percentile as PERCENTILE
from numpy import uint16
from numpy import uint8
from numpy import zeros
from numpy.random import RandomState
from numpy.typing import NDArray
from shapely.geometry import Polygon

from localised.thresher.cfg import HAZARDLY_THRESHOLD_DIMENSIONS
from localised.thresher.cfg import LAT
from localised.thresher.cfg import LAYER
from localised.thresher.cfg import LON
from localised.thresher.cfg import MASK
from localised.thresher.cfg import TIME
from localised.thresher.errors import ThresherError
from localised.thresher.netcdf import Netcdf
from localised.thresher.netcdf import get_attr
from localised.utils.rescale import Masking


class Percentile:
    """Stores percentile parameters for the threshold calculation.

    Attributes:
        var: The referenced NetCDF variable.
        op: The comparison operator.
        p: The percentile, should be between 0-100.
    """
    regex = re.compile(r' ?(?P<var>[\w\-_]+) ?(?P<op>>|<|<=|>=) ?(?P<percentile>(\d+)?\.?\d+) ?')

    def __init__(self, var: str, op: str, percentile: float) -> None:
        if percentile <= 0 or percentile >= 100:
            raise ThresherError('Should be between 0 and 100')

        self.var = var
        self.op = op
        self.p = percentile

    def compare(self, responses: NDArray[float], thresholds: NDArray[float]) -> NDArray[int]:
        """Compare responses with thresholds by applying the initialized comparison operator.

        Both parameters should have the same dimensionality.

        Args:
            responses: A responses dataset.
            thresholds: A thresholds dataset.

        Returns:
            The binary result of the comparison where zero indicates false and one true.
        """
        if self.op == '>':
            return responses > thresholds
        elif self.op == '>=':
            return responses >= thresholds
        elif self.op == '<':
            return responses < thresholds
        elif self.op == '<=':
            return responses <= thresholds
        else:
            raise ThresherError(f'Should never happen')

    def evaluate(self, data: NDArray[float]) -> NDArray[float]:
        """Get the initialized percentile for a set of samples.

        The percentile is always evaluated for the zeroth axis.

        Args:
            data: A set of samples.

        Returns:
            The percentiles of the provided samples.
        """
        return PERCENTILE(data, q=self.p, axis=0)

    def __repr__(self) -> str:
        return '<{}({}, {}, {}) at {}>'.format(__class__.__name__, self.var, self.op, self.p, hex(id(self)))

    def __str__(self) -> str:
        return '{}_{}_P{}'.format(self.var, self.op, self.p)

    @classmethod
    def from_text(cls, text: str) -> 'Percentile':
        """Init a class instance via a "percentile string" with the following grammar:

        percentile = variable, operator, int | float ;
        variable = letter | digit | symbol, { letter | digit | symbol } ;
        operator = "<" | "<=" | ">" | ">=" ;
        int = digit, { digit } ;
        float = digit, { digit }, dot, { digit } ;
        dot = "." ;
        digit = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" ;
        symbol = "-" | "_" ;
        letter =  "A" | "B" | "C" | "D" | "E" | "F" | "G"
                | "H" | "I" | "J" | "K" | "L" | "M" | "N"
                | "O" | "P" | "Q" | "R" | "S" | "T" | "U"
                | "V" | "W" | "X" | "Y" | "Z" | "a" | "b"
                | "c" | "d" | "e" | "f" | "g" | "h" | "i"
                | "j" | "k" | "l" | "m" | "n" | "o" | "p"
                | "q" | "r" | "s" | "t" | "u" | "v" | "w"
                | "x" | "y" | "z" ;
        """
        m = cls.regex.match(text)

        if not m:
            raise ThresherError('Illegal formatting')

        return cls(m.group('var'), m.group('op'), float(m.group('percentile')))


class Time:
    """Threshold timestamp management.

    Attributes:
        base: The base period (Start base year, End base year).
        months: A month selection, values must be between 1-12.
        window: Size of the sub-sampling window, must be an odd positive integer and preferred is a window size of
            window * base > 100.
        hour: Set only to non-default value if all the timestamps in your target dataset refer to non-midnight
            hours.
        minute: Set only to non-default value if all the timestamps in your target dataset refer to non-midnight
            minutes.
        microsecond: Set only to non-default value if all the timestamps in your target dataset refer to
            non-midnight microseconds.
        seed: Set it only if you want to achieve deterministic random year generation for bootstrapping.
    """

    def __init__(
            self,
            base: Tuple[int, int],
            months: List[int],
            window: int,
            hour: int = 0,
            minute: int = 0,
            microsecond: int = 0,
            seed: int = None,
    ) -> None:
        if window % 2 == 0:
            raise ThresherError('Window size must be odd')

        if min(months) < 1 or max(months) > 12:
            raise ThresherError('Illegal month, must be between 1-12')

        if base[1] < base[0]:
            raise ThresherError('Start base year must be smaller than end base year')

        self.seed = seed

        self.window = window
        self.base_start, self.base_end = base
        self.months = months

        self.hour = hour
        self.minute = minute
        self.microsecond = microsecond

        self._ra = RandomState(self.seed)

    @cached_property
    def month_str(self) -> str:
        """Get months as a comma separated list."""
        return ','.join(map(str, self.months))

    @cached_property
    def base_length(self) -> int:
        """Get the length of the base period."""
        return self.base_end - self.base_start + 1

    @cached_property
    def years(self) -> List[int]:
        """Get a list of all base years."""
        return list(range(self.base_start, self.base_end + 1))

    @cached_property
    def indices(self) -> List[int]:
        """Get a list of day indices for all initialized months.

        Please note, the day indices ignores leap years, therefore it returns always for an entire year a list where all
        values are between 1-365.

        Examples:
            >>> t = Time(base=(1980, 1981), months=[2], window=5)
            >>> t.indices
        [32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59]
        """
        sink = list()
        for m in self.months:
            start, end = self.monthly_indices[m]
            sink += list(range(start, end + 1))

        return sink

    @cached_property
    def monthly_indices(self) -> Dict[int, Tuple[int, int]]:
        """Get the start and end day index for each month in a year.

        Please note, for February the monthly indices is always (32, 59).

        Returns:
            The start and end index day of month each month in a year as dictionary of tuples with two elements. Key is
            the numeric identifier of each month [1, 12]. The first and second element of the tuple is the start and end
            day index for the corresponding month.
        """
        end = 0
        months = dict()

        for month in range(1, 13):
            start = end
            m = monthrange(self.base_start, month)[1]
            if month == 2:
                m = 28
            end += m
            months[month] = (start + 1, end)

        return months

    @cached_property
    def sampling_range(self) -> List[DatetimeNoLeap]:
        """Get the timestamp sampling range as a list of timestamps.

        Please note, sampling range is always limited to all days within the initialized months (with window overshoot).
        The year of the returned timestamps is always the start of the base period.
        """
        sink = list()

        for m in self.months:
            for d in range(1, 32):
                try:
                    d = DatetimeNoLeap(year=self.base_start, month=m, day=d,
                                       hour=self.hour, minute=self.minute, microsecond=self.microsecond)
                    sink.append(d)
                except ValueError:
                    break

        return sink

    def reseed(self, seed: Union[None, int]) -> None:
        """Set a new seed for the random number generator.

        Args:
            seed: A positive integer, if you set None as seed the random generator will not operate with a specific
                seed.
        """
        self._ra = RandomState(seed)
        self.seed = seed

    def timestamp_index(self, timestamp: datetime) -> int:
        """Get the day index of a timestamp.

        Raises:
            ThresherError: If the month of the provided parameter is not within the initialized months.
        """
        if timestamp.month not in self.months:
            raise ThresherError('Not within selected months')

        return self.monthly_indices[timestamp.month][0] + timestamp.day - 1

    def index(self, day_index: int) -> int:
        """Get the list index of the day index.

        Raises:
            ThresherError: If the provided day index is not within the initialized months.
        """
        try:
            return self.indices.index(day_index)
        except ValueError:
            raise ThresherError('Not within selected months')

    def within(self, netcdf: Netcdf, bootstrapped: bool) -> List[float]:
        """Selects from the Netcdf time dimension all timestamps within the initialized months and converts them
        to the numeric representation. If bootstrapped is set to true only timestamps within the base period are
        selected.

        Args:
            netcdf: An object that provides a time dimension.
            bootstrapped: If true selects only timestamps within the base-period.

        Returns:
            A list of date-time stamps in numerical representation.
        """
        selection = list()

        for dt in netcdf.dates:
            if bootstrapped and dt.year not in self.years:
                continue
            if dt.month in self.months and not (dt.month == 2 and dt.day == 29):
                selection.append(dt)

        return date2num(selection, units=netcdf.tunits, calendar=netcdf.tcalendar)

    def samples(self, keep: List[int] = None) -> Tuple[DatetimeNoLeap, List[DatetimeNoLeap]]:
        """Timestamp samples generator function.

        Creates for each timestamp of the initialized months within the first base year a list of timestamps
        over the entire base period by adding timestamps within the window, e.g., window is 3, base period is
        2019, 2020, and months is February (2) then you obtain:

        1: 2019.2.1, 2019.1.31, 2019.2.2, 2020.2.1, 2020.1.31, 2020.2.2
        2: 2019.2.2, 2019.2.1, 2019.2.3, 2020.2.2, 2020.2.1, 2020.2.3
        ...

        If keep is set, will randomly select years from keep till the length of keep plus randomly selected
        years will be equal to the base length (see Zhang, X., Hegerl, G., Zwiers, F. W., & Kenyon, J. (2005). Avoiding
        inhomogeneity in percentile-based indices of temperature extremes. Journal of Climate, 18(11), 1641–1651.
        https://doi.org/10.1175/JCLI3366.1).

        Example:
        obj = Time(base=(2000, 2004), months=[2], window=3)
        obj.samples(keep=[2002, 2003, 2004]) ->
            (Date(2002, 2, 1),
            [Date(2002, 2, 1), Date(2002, 1, 31), Date(2002, 2, 2),
            Date(2003, 2, 1), Date(2003, 1, 31), Date(2003, 2, 2),
            Date(2004, 2, 1), Date(2004, 1, 31), Date(2004, 2, 2),
            Date(2002, 2, 1), Date(2002, 1, 31), Date(2002, 2, 2),
            Date(2004, 2, 1), Date(2004, 1, 31), Date(2004, 2, 2)])

        Args:
            keep: Years to keep for randomization.

        Returns:
            A tuple where the first element is the timestamp of the current day within the first base year in
            process and the second element is a list of date-time stamps representing all days till the end base year
            where day and months equals current plus a window centred at each timestamp.
        """
        repeat = self.randomize(keep) if keep else list()

        for date in self.sampling_range:
            # Create dates over the entire base period, e.g., date = 2020.1.1 create=2021.1.1, 2022.1.1, ...
            tmp = [date + timedelta(days=i * 365) for i in range(self.base_length)]

            # Only keep dates where the year equals years listed in keep and add dates with random selected years from
            # repeat.
            if repeat:
                tmp = [d for d in tmp if d.year in keep]
                t = tmp[-1]
                tmp += [t.__class__(y, t.month, t.day, t.hour, t.minute, t.microsecond) for y in repeat]

            # Create window centred around each date time
            selection = list()
            for d in tmp:
                selection.append(d)
                for j in range(1, ceil(self.window / 2)):
                    selection.append(d + timedelta(days=-j))
                    selection.append(d + timedelta(days=j))

            yield selection[0], selection

    def randomize(self, keep: List[int]) -> List[int]:
        """Selects random years from keep till the length of keep plus random selected years is equal to the base
        length.

        Returns:
            A list of random selected years.
        """
        return self._ra.choice(keep, size=self.base_length - len(keep)).tolist()

    def __len__(self) -> int:
        """Get the number of days for one base year."""
        return len(self.indices)

    def __repr__(self) -> str:
        return '<{}(({}, {}), {}, {}) at {}>'.format(self.__class__.__name__, self.base_start, self.base_end,
                                                     self.window, self.months, hex(id(self)))

    def __str__(self) -> str:
        return '{}-{}_{}CD_{}'.format(self.base_start, self.base_end, self.window, self.month_str)


class Threshold:
    """This class computes daily thresholds for studies that like to apply the percentile-based extremes methodology
    introduced by:

    Zhang, X., Hegerl, G., Zwiers, F. W., & Kenyon, J. (2005). Avoiding inhomogeneity in percentile-based indices of
    temperature extremes. Journal of Climate, 18(11), 1641–1651. https://doi.org/10.1175/JCLI3366.1

    Please use the create factory if you want to construct a new threshold dataset and use the get factory for already
    existing datasets.
    """

    def __init__(self, path: Path) -> None:
        self.path = path

        with Dataset(self.path) as nc:
            for d in HAZARDLY_THRESHOLD_DIMENSIONS:
                if not (d in nc.dimensions or d in nc.variables):
                    raise ThresherError('Missing dimension %s' % d)

            time = nc.variables[TIME]
            self.time = Time(
                base=(time.start, time.end),
                months=list(map(int, time.months.split(','))),
                window=time.window,
                hour=time.hour,
                minute=time.minute,
                microsecond=time.microsecond,
                seed=get_attr('seed', time)
            )

            layer = nc.variables[LAYER]
            self._bootstrapped: bool = bool(layer.bootstrapped)
            self._repetitions: int = layer.repetitions
            self._order: int = layer.order
            self.created: bool = bool(layer.created)
            self.layer: NDArray[int] = layer[:]

            self.lats: NDArray[float] = nc.variables[LAT][:]
            self.lons: NDArray[float] = nc.variables[LON][:]
            self.mask: NDArray[int] = nc.variables[MASK][:, :]

            self.percentiles: List[Percentile] = [
                Percentile(k, v.op, v.p)
                for k, v in nc.variables.items()
                if k not in HAZARDLY_THRESHOLD_DIMENSIONS
            ]

            self._src: Union[None, Path] = None
            if not self.created:
                self._src = Netcdf(Path(layer.created_from))

    @property
    def bootstrapping(self) -> bool:
        """Always false for non bootstrapped thresholds."""
        return self._bootstrapped

    @property
    def repetitions(self) -> int:
        """Always one for non bootstrapped thresholds."""
        return self._repetitions

    @property
    def order(self) -> int:
        """Always zero for non bootstrapped thresholds."""
        return self._order

    @cached_property
    def years(self) -> List[int]:
        """Get a list of all base years."""
        return self.time.years

    def within(self, netcdf: Netcdf) -> List[float]:
        """See Time.within documentation."""
        return self.time.within(netcdf, self.bootstrapping)

    def percentiles_str(self) -> str:
        """Get all percentiles in text representation."""
        return ' AND '.join(map(str, self.percentiles))

    def time_str(self) -> str:
        """Get threshold base period in text representation."""
        return str(self.time)

    def run(self) -> 'Threshold':
        """Executes the threshold calculation."""
        if self.created:
            return self

        with progressbar(list(self.time.samples()), label='Computing thresholds') as timestamps:
            for current, samples in timestamps:
                for percentile in self.percentiles:
                    data = self._src.read(percentile.var, samples, self.lats, self.lons)
                    self._write(percentile.var, 0, current, percentile.evaluate(data))

        return self

    def read(
            self,
            var: str,
            layer: int,
            date: datetime,
            lat: Union[Number, List[Number]],
            lons: Union[Number, List[Number]] = None
    ) -> NDArray[float]:
        """Returns thresholds.

        Args:
            var: The variable to fetch from.
            layer: The layer to fetch from.
            date: The timestamp of the data.
            lat: The latitude coordinates to read.
            lons: The longitude coordinates, if not set data for entire range of longitude coordinates is returned.

        Returns:
            Thresholds
        """
        with Dataset(self.path) as nc:
            v = nc.variables[var]
            if lons is None:
                return v[layer, self.time.index(self.time.timestamp_index(date)), isin(self.lats, lat), :]
            return v[
                layer, self.time.index(self.time.timestamp_index(date)), isin(self.lats, lat), isin(self.lons, lons)]

    # noinspection PyProtectedMember
    def _write(self, var: str, layer: int, timestamp: datetime, thresholds: NDArray[float]) -> None:
        """Write on disk."""
        with Dataset(self.path, mode='a') as nc:
            var = nc.variables[var]
            thresholds[self.mask == True] = var._FillValue
            var[layer, self.time.index(self.time.timestamp_index(timestamp)), :, :] = thresholds


class ThresholdBootstrapped(Threshold):
    """This class computes daily bootstrapped thresholds by applying the approach described by Zhang et al., 2005."""

    # noinspection PyTypeChecker
    def run(self) -> 'ThresholdBootstrapped':
        """Executes the bootstrapped threshold calculation."""
        if self.created:
            return self

        with progressbar(list(zip(self.layer, self.sampling_years())),
                         label='Computing bootstrapped thresholds') as lay_yrs:
            for lay, yrs in lay_yrs:
                for current, samples in self.time.samples(keep=yrs):
                    for percentile in self.percentiles:
                        data = self._src.read(percentile.var, samples, self.lats, self.lons, bootstrapped=True)
                        self._write(percentile.var, lay, current, percentile.evaluate(data))

        return self

    def sampling_years(self) -> List[int]:
        """Method creates per call a list of years that may be used for bootstrapped sampling.

        If order is 0 first current base year is removed first and after each year from left to right.
        If order is -1 first current base year is removed first and after each year from right to left.

        Example:
            order, current = 0, 1970
            [1970 1971 1972 1973 1974]
            [1971 1972 1973 1974]
            [1972 1973 1974]
            ...
            order, current = -1, 1970
            [1970 1971 1972 1973 1974]
            [1971 1972 1973 1974]
            [1971 1972 1973]
            ...

        Returns:
            Per call a list of years that should be used for bootstrapped sampling.
        """
        years = self.time.years
        current = -1
        selected = list()

        for lay in self.layer:
            if lay // self.repetitions != current:
                selected = copy(years)
                current = lay // self.repetitions
                selected.pop(current)
            else:
                selected.pop(self.order)

            yield copy(selected)


def get(path: Path) -> Union[Threshold, ThresholdBootstrapped]:
    """Get a Threshold instance, the type of the instance depends on the provided dataset.

    Args:
        path: Path to the threshold dataset

    Returns:
        An threshold instance either Threshold or ThresholdBootstrapped
    """
    with Dataset(path) as nc:
        if nc.variables[LAYER].bootstrapped:
            instance = ThresholdBootstrapped
        else:
            instance = Threshold

    return instance(path)


def create(
        path: Path,
        netcdf: Path,
        percentiles: List[str],
        years: Tuple[int, int],
        months: List[int],
        window: int = 5,
        bootstrapped: bool = False,
        repetitions: int = 1,
        order: int = 0,
        seed: int = None,
        roi: Polygon = None,
) -> Union[Threshold, ThresholdBootstrapped]:
    """Creates a new threshold dataset and returns a threshold instance ready for a computation run.

    Args:
        path: Path were the new threshold dataset should be created.
        netcdf: Source data NetCDF, required for init of time, latitude, and longitude dimension.
        percentiles: The threshold percentiles as a percentile string, please check the thresholds cli documentation.
        years: Base period of the thresholds, the first element must be the starting year and the second the end year.
        months: The months that should be considered for the threshold generation.
        window: Size of the subsampling window.
        bootstrapped: If set the thresholds will be calculated by applying bootstrapping.
        repetitions: he number of repetitions per base year, if bootstrapping is selected.
        order: Order of year removal per bootstrapping run.
        seed: Define a seed for the random number generator.
        roi: Geometric mask

    Returns:
        An threshold instance either Threshold or ThresholdBootstrapped
    """
    if path.is_file():
        return get(path)

    nc = Netcdf(netcdf)
    time = Time(years, months, window, nc.hour, nc.minute, nc.microsecond, seed)
    percentiles = [Percentile.from_text(text) for text in percentiles]

    if roi:
        m = Masking(nc.lats, nc.lons, all_touched=True)
        lats, lons = m.masked_lats(roi), m.masked_lons(roi)
        mask = Masking(lats, lons, all_touched=True).get_mask(roi)
    else:
        lats, lons, mask = nc.lats, nc.lons, zeros((len(nc.lats), len(nc.lons)))

    if bootstrapped:
        if repetitions < 0:
            repetitions = 1
        elif repetitions >= time.base_length:
            repetitions = time.base_length - 1
        if order > 0:
            order = 0
        elif order < 0:
            order = -1
        layers = list(range(0, time.base_length * repetitions))
        bootstrapped, repetitions, order = int(bootstrapped), repetitions, order
        instance = ThresholdBootstrapped
    else:
        bootstrapped, layers, repetitions, order = int(bootstrapped), [0], 1, 0
        instance = Threshold

    with Dataset(path, mode='w') as threshold:
        threshold.createDimension(LAYER, len(layers))
        lay = threshold.createVariable(LAYER, uint16, (LAYER,), fill_value=False)
        lay[:] = layers
        lay.bootstrapped, lay.repetitions, lay.order = bootstrapped, repetitions, order
        lay.created, lay.created_from = int(False), str(nc.path)

        threshold.createDimension(TIME, len(time))
        t = threshold.createVariable(TIME, int32, (TIME,), fill_value=False)
        t[:] = time.indices
        t.window, t.start, t.end, = time.window, time.base_start, time.base_end
        t.months = time.month_str
        t.hour, t.minute, t.microsecond = time.hour, time.minute, time.microsecond
        if time.seed:
            t.seed = seed

        threshold.createDimension(LAT, len(lats))
        lat = threshold.createVariable(LAT, float64, (LAT,), fill_value=False)
        lat[:] = lats

        threshold.createDimension(LON, len(lons))
        lon = threshold.createVariable(LON, float64, (LON,), fill_value=False)
        lon[:] = lons

        m = threshold.createVariable(MASK, uint8, (LAT, LON), fill_value=False)
        m[:, :] = mask

        for percentile in percentiles:
            if percentile.var not in nc.vars:
                raise ThresherError(f'{percentile.var} not available in {str(nc.path)}')

            var = threshold.createVariable(percentile.var, float64, (LAYER, TIME, LAT, LON),
                                           fill_value=nc.fill_value(percentile.var))
            var.p = percentile.p
            var.op = percentile.op
            var.units = nc.units(percentile.var)
            var.standard_name = get_attr('standard_name', var, default='NA')
            var.long_name = get_attr('long_name', var, default='NA')

    return instance(path)
