from functools import cached_property
from functools import reduce
from numbers import Number
from pathlib import Path
from typing import List
from typing import Tuple
from typing import Union

from cftime import datetime
from click import progressbar
from netCDF4 import Dataset
from netCDF4 import num2date
from numpy import float64
from numpy import isin
from numpy import logical_and
from numpy import uint16
from numpy import uint8
from numpy.typing import NDArray

from localised.thresher.cfg import EXCEED
from localised.thresher.cfg import HAZARDLY_EXCEEDANCE_DIMENSIONS
from localised.thresher.cfg import LAT
from localised.thresher.cfg import LAYER
from localised.thresher.cfg import LON
from localised.thresher.cfg import TIME
from localised.thresher.errors import ThresherError
from localised.thresher.netcdf import Netcdf
from localised.thresher.threshold import get as get_threshold


class Exceedance:
    """Class for computing daily exceedance from a threshold and source data NetCDF."""

    def __init__(self, path: Path) -> None:
        self.path = path

        with Dataset(self.path) as nc:
            for d in HAZARDLY_EXCEEDANCE_DIMENSIONS:
                if d not in nc.dimensions:
                    raise ThresherError(f'Missing dimension {d}')

            layer = nc.variables[LAYER]
            self.layer = layer[:]
            self.bootstrapped = bool(layer.bootstrapped)
            self.repetitions = layer.repetitions
            self.order = layer.order

            time = nc.variables[TIME]
            self.units = time.units
            self.calendar = time.calendar
            self.dates = num2date(time[:], units=self.units, calendar=self.calendar)
            self.base = time.base

            self.lats = nc.variables[LAT][:]
            self.lons = nc.variables[LON][:]

            exceed = nc.variables[EXCEED]
            self.created = bool(exceed.created)
            self.percentiles = exceed.percentiles
            self._responses, self._thresholds = None, None

            if not self.created:
                self._responses = Netcdf(Path(exceed.responses))
                self._thresholds = get_threshold(Path(exceed.thresholds))

    @property
    def days(self) -> List[datetime]:
        """Get which days are covered by this exceedance dataset."""
        return self.dates

    @cached_property
    def months(self) -> List[datetime]:
        """Get which months are covered by this exceedance dataset."""
        months = [self.dates[0]]
        for m in self.dates:
            if months[-1].month != m.month:
                months.append(m)
            elif months[-1].month == m.month and months[-1].year != m.year:
                months.append(m)
        return months

    @cached_property
    def years(self) -> List[datetime]:
        """Get which years are covered are covered by this exceedance dataset."""
        years = [self.dates[0]]
        for t in self.dates:
            if t.year != years[-1].year:
                years.append(t)
        return years

    def run(self) -> 'Exceedance':
        """Compute daily exceedance"""
        if self.created:
            return self

        with progressbar(self.dates, label='Computing exceedance') as timestamps:
            for timestamp in timestamps:
                data = list()
                for percentile in self._thresholds.percentiles:
                    data.append(percentile.compare(
                        responses=self._responses.read(percentile.var, timestamp, self.lats, self.lons),
                        thresholds=self._thresholds.read(percentile.var, 0, timestamp, self.lats, self.lons),
                    ))

                self._write(0, timestamp, reduce(lambda previous, current: previous & current, data))

        return self

    def read(
            self,
            layer: int,
            span: Tuple[datetime, datetime],
            lat: Union[Number, List[Number]],
            lon: Union[Number, List[Number]] = None,
    ) -> NDArray[int]:
        """Read daily exceedance for a selected layer, year, and latitude coordinates. If longitude coordinates are
        provided only data within lat, lon is returned. If month is set only data for the selected month is returned.
        If month and year are set month takes precedence over year (exceedance for the selected month is returned
        not for the selected year).

        Args:
            layer: The layer to read from
            span: The time span data to select from
            lat: The latitude coordinate
            lon: The longitude coordinate (optional due to compatibility)
        """
        s, e = span
        with Dataset(self.path) as nc:
            ex = nc.variables[EXCEED]
            if lon is None:
                return ex[layer, logical_and(self.dates >= s, self.dates < e), isin(self.lats, lat), :]
            return ex[layer, logical_and(self.dates >= s, self.dates < e), isin(self.lats, lat), isin(self.lons, lon)]

    def _write(self, layer: int, timestamp: datetime, data: NDArray[int]) -> None:
        """Writes results to exceedance dataset"""
        with Dataset(self.path, mode='a') as nc:
            nc.variables[EXCEED][layer, self.dates == timestamp, :, :] = data


class ExceedanceBootstrapped(Exceedance):
    """Class for computing bootstrapped exceedance."""

    def run(self) -> 'ExceedanceBootstrapped':
        """Execute the bootstrapped exceedance computation."""
        if self.created:
            return self

        with progressbar(self.layer, label='Computing bootstrapped exceedance') as layer:
            for lay in layer:
                yr = self._thresholds.years[lay // self._thresholds.repetitions]
                for timestamp in self.dates:
                    if timestamp.year == yr:
                        data = list()
                        for percentile in self._thresholds.percentiles:
                            data.append(percentile.compare(
                                responses=self._responses.read(percentile.var, timestamp, self.lats, self.lons),
                                thresholds=self._thresholds.read(percentile.var, lay, timestamp, self.lats, self.lons),
                            ))

                        self._write(lay, timestamp, reduce(lambda previous, current: previous & current, data))

        return self


def get(path: Path) -> Union[Exceedance, ExceedanceBootstrapped]:
    """Get an Exceedance instance, the type of the instance depends on the provided dataset.

    Args:
        path: Path to exceedance dataset.

    Returns:
        An exceedance instance either Exceedance or ExceedanceBootstrapped.
    """
    with Dataset(path) as nc:
        if nc.variables[LAYER].bootstrapped:
            instance = ExceedanceBootstrapped
        else:
            instance = Exceedance

    return instance(path)


# noinspection DuplicatedCode
def create(path: Path, threshold: Path, netcdf: Path) -> Union[Exceedance, ExceedanceBootstrapped]:
    """Creates a new exceedance dataset and returns an Exceedance or ExceedanceBootstrapped instance.

    Args:
        path: Path where the new exceedance dataset should be created.
        threshold: Threshold dataset, required for calculating the exceedance.
        netcdf: Source data NetCDF, exceedance will be determined by comparing data from this dataset with the provided
            threshold dataset.

    Returns:
        An Exceedance instance, either Exceedance or Exceedance bootstrapped.
    """
    if path.is_file():
        return get(path)

    threshold = get_threshold(threshold)
    layers, lats, lons = threshold.layer, threshold.lats, threshold.lons

    netcdf = Netcdf(netcdf)
    times, units, calendar = threshold.within(netcdf), netcdf.tunits, netcdf.tcalendar

    if threshold.bootstrapping:
        instance = ExceedanceBootstrapped
    else:
        instance = Exceedance

    with Dataset(path, mode='w') as exceedance:
        exceedance.createDimension(LAYER, len(layers))
        layer = exceedance.createVariable(LAYER, uint16, (LAYER,), fill_value=False)
        layer[:] = layers
        layer.bootstrapped = int(threshold.bootstrapping)
        layer.repetitions = threshold.repetitions
        layer.order = threshold.order

        exceedance.createDimension(TIME, len(times))
        time = exceedance.createVariable(TIME, float64, (TIME,), fill_value=False)
        time[:] = times
        time.units, time.calendar = units, calendar
        time.base = threshold.time_str()

        exceedance.createDimension(LAT, len(lats))
        latitude = exceedance.createVariable(LAT, float64, (LAT,), fill_value=False)
        latitude[:] = lats

        exceedance.createDimension(LON, len(lons))
        longitude = exceedance.createVariable(LON, float64, (LON,), fill_value=False)
        longitude[:] = lons

        exceedance = exceedance.createVariable(EXCEED, uint8, (LAYER, TIME, LAT, LON), fill_value=0)
        exceedance.created = int(False)
        exceedance.responses = str(netcdf.path)
        exceedance.thresholds = str(threshold.path)
        exceedance.percentiles = threshold.percentiles_str()

    return instance(path)
