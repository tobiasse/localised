from pathlib import Path

import click

from localised.thresher.exceedance import create


@click.command()
@click.argument('netcdf', required=True, type=click.Path(exists=True))
@click.argument('threshold', required=True, type=click.Path(exists=True))
@click.argument('exceedance', required=True, type=click.Path())
def apply(netcdf: str, threshold: str, exceedance: str) -> None:
    """Apply computes the daily exceedance (EXCEEDANCE) index for a NetCDF (NETCDF) and the corresponding thresholds
    (THRESHOLD).

    The provided NetCDF should comprise all data variables that are present in the threshold dataset. Further, the
    NetCDF must fully cover the time dimension of the threshold data, i.e., if the threshold dataset comprises
    thresholds for May, the NetCDF should provide at least data for the same month. Dates without the threshold time
    coverage will be ignored and not considered for the resulting exceedance dataset. The algorithm fetches the rules
    for an exceedance from the threshold dataset, i.e., threshold percentile definitions are tmpmin>90,tmpmax>90 then
    the exceedance is determined by evaluating tmpmin>Threshold(tmpmin, day, x, y) AND
    tmpmax>Threshold(tmpmax, day, x, y).
    """
    create(Path(exceedance), Path(threshold), Path(netcdf)).run()
