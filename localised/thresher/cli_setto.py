from typing import List

import click

from localised.thresher.netcdf import set_to
from localised.utils.parse import parse_ranges


# noinspection SpellCheckingInspection
@click.command()
@click.option('-v', '--variable', type=str, required=True, help='Variable selection')
@click.option('-m', '--months', type=str, required=True, callback=parse_ranges(),
              help='Month selection, e.g., 3, 3-5,6')
@click.option('-c', '--constant', type=int, required=True, help='A numeric constant')
@click.argument('netcdf', type=str, required=True)
def setto(variable: str, months: List[str], constant: int, netcdf: str) -> None:
    """Sets values of a selected NetCDF (NETCDF) variable (-v, --variable) to a constant (-c, --constant) value for all
    dates within the selected months (-m, --months).
    """
    set_to(netcdf, variable, list(map(int, months)), constant)
