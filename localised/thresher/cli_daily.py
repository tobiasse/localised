from pathlib import Path

import click

from localised.thresher.aggregation import create


@click.command()
@click.option('--confidence', type=float, default=.75, show_default=True, help='')
@click.argument('exceedance', required=True, type=click.Path(exists=True))
@click.argument('days', required=True, type=click.Path())
def daily(confidence: float, exceedance: str, days: str) -> None:
    """Aggregates daily exceedance index (EXCEEDANCE) to daily aggregated exceedance index (DAYS) by summing each grid
    cell calculates the statistical mean or majority for bootstrapped daily exceedance.
    """
    create(Path(days), Path(exceedance), steps='daily', confidence=confidence).run()
