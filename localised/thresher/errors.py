class ThresherError(Exception):
    """Raise if some exception occurs during the execution of a thresher module"""
