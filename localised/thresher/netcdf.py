from numbers import Number
from pathlib import Path
from typing import Any
from typing import List
from typing import Sequence
from typing import Union

import numpy as np
from cftime import datetime
from netCDF4 import Dataset
from netCDF4 import Variable
from netCDF4 import num2date
from numpy import array
from numpy import asarray
from numpy import finfo
from numpy import iinfo
from numpy import isin
from numpy import unique
from numpy import where
from numpy.ma import append

from localised.thresher.cfg import DIMENSIONS
from localised.thresher.cfg import LAT
from localised.thresher.cfg import LAYER
from localised.thresher.cfg import LON
from localised.thresher.cfg import TIME
from localised.thresher.errors import ThresherError


def copy_to(src: Union[str, Path], dst: Union[str, Path], variable: str) -> None:
    """Copies the values of src to dst. src values take precedence over dst values.

    Args:
        src: Source NetCDF
        dst: Destination NetCDF
        variable: The variable to copy
    """
    with Dataset(src) as src, Dataset(dst, mode='a') as dst:
        assert variable in src.variables
        assert variable in dst.variables
        assert TIME in src.variables
        assert LON in src.variables
        assert LAT in src.variables
        assert LAYER not in src.variables
        assert TIME in dst.variables
        assert LON in dst.variables
        assert LAT in dst.variables
        assert LAYER not in dst.variables

        ori = src[variable]
        tar = dst[variable]
        fill = fill_value(ori)

        assert len(ori) == len(tar)

        for i in range(len(ori)):
            ori_data = ori[i, :, :]
            tar_data = tar[i, :, :]
            tar_data = where(ori_data.data != fill, ori_data, tar_data)
            tar[i, :, :] = tar_data


def set_to(nc: Union[str, Path], variable: str, months: List[int], constant: Number) -> None:
    """Sets values of a selected NetCDF variable to a constant value for all dates within the selected months.

    Args:
        nc: Path to the NetCDF.
        variable: The selected variable.
        months: Constant is applied to all dates for the selected months.
        constant: The constant value.
    """
    with Dataset(nc, mode='a') as src:
        assert variable in src.variables
        assert TIME in src.variables
        assert LON in src.variables
        assert LAT in src.variables
        assert LAYER not in src.variables

        dates = num2date(src.variables[TIME][:], units=src.variables[TIME].units, calendar=src.variables[TIME].calendar)

        for i, d in enumerate(dates):
            if d.month in months:
                src.variables[variable][i, :, :] = constant


# noinspection PyProtectedMember,PyBroadException
def fill_value(var: Variable) -> Union[None, Number]:
    """Get fill value of a netCDF variable.

    If the attribute _FillValue or missing_value is not set for the variable the functions reads the first value from
    the variable and checks if the returned ndarray (ma.ndarray) provides a fill value. If all these three checks do
    not provide a fill value the function returns the minimum value of the variable data type.

    Args:
        var: A netCDF variable

    Returns:
        Either the fill value of the variable if set or the default value if not set.
    """
    dtype = var.dtype

    try:
        default = iinfo(dtype).min
    except ValueError:
        default = finfo(dtype).min

    try:
        return var._FillValue
    except AttributeError:
        try:
            return var.missing_value
        except AttributeError:
            try:
                return var[0].fill_value
            except Exception:
                return default


def get_attr(attr: str, var: Variable, default: Any = None) -> Any:
    """Get safely an attribute from a netCDf variable.

    Args:
        attr: The attribute to fetch.
        var: The netCDF variable
        default: The value to return if attribute does not exist.

    Returns:
        None or the attribute value if set.
    """
    try:
        return var.getncattr(attr)
    except AttributeError:
        return default


class Netcdf:
    """This class is used to access NetCDF source data for calculation of thresholds and exceedance.

    Attributes:
        path: Path to the netCDf file.
        dates: The time dimension of the netCDF.
        lats: The latitude dimension of the netCDF.
        lons: The longitude dimension of the netCDF.
        vars: The netCDF variables except time, latitude, and longitude.
        tunits: Unit of the time dimension.
        tcalendar: Calendar of the time dimension.
    """
    DIMENSIONS = ['time', 'latitude', 'longitude']

    def __init__(self, path: Path) -> None:
        if not path.is_file():
            raise ThresherError('%s is not a file' % path)

        self.path = path

        with Dataset(self.path) as nc:
            for d in DIMENSIONS:
                if d not in nc.dimensions:
                    raise ThresherError('Missing dimension %s' % d)

            d = nc.variables[TIME][:]
            c = get_attr('calendar', nc.variables[TIME], 'standard')
            if u := get_attr('units', nc.variables[TIME]):
                self.dates = num2date(d, units=u, calendar=c)
                self.tunits = u
                self.tcalendar = c
            else:
                raise ThresherError(f'attr units missing in var {TIME}')

            first = self.dates[0]
            self.hour, self.minute, self.microsecond = first.hour, first.minute, first.microsecond

            self.lats = nc.variables[LAT][:]
            self.lons = nc.variables[LON][:]
            self.vars = [k for k in nc.variables if k not in self.__class__.DIMENSIONS]

    def read(
            self,
            var: str,
            dates: Union[datetime, List[datetime]],
            lats: Union[int, float, Sequence[Union[float, int]]],
            lons: Union[int, float, Sequence[Union[float, int]]],
            bootstrapped: bool = False
    ) -> array:
        """Reads netCDF data from the selected variable for the defined selection of time, latitude, and longitude.

        Supports bootstrapped read by setting bootstrapped to TRUE (length of data will be equal to the size of dates).
        A bootstrapped read occurs when some dates in dates are duplicated. Usually, the returned data would contain the
        data for this date only once. In the case of a bootstrapped read, the data for the repeated date gets duplicated
        (as often as the specific date appears in dates).

        Example:
        nc.read('', [Date(1970, 2, 1), Date(1970, 2, 1), Date(1970, 2, 1)], 52., 2.) -> [[[60.]]]

        nc.read('', [Date(1970, 2, 1), Date(1970, 2, 1), Date(1970, 2, 1)], 52., 2., True) -> [[[60.]], [[60]], [[60]]]

        Args:
            var: The variable to read from. Should be a variable with 3D index consisting of time, latitude, and
                longitude.
            dates: A date time selection.
            lats: One or multiple latitude coordinates to select.
            lons: One or multiple longitude coordinates to select.
            bootstrapped: If set to true the length of the returned data will be equal the length of dates (duplicated
                dates will result in duplicated data).

        Returns:
            The netCDF data for the selected variable and the selected index.
        """
        if var not in self.vars:
            raise ThresherError('Unknown variable %s' % var)

        dates = array(self._cast(dates))

        with Dataset(self.path) as nc:
            data = nc.variables[var][isin(self.dates, dates), isin(self.lats, lats), isin(self.lons, lons)]

            if bootstrapped:
                # Select only dates available in NetCDF (Prevents duplication of non-existent dates)
                dates = dates[np.isin(dates, self.dates)]
                # Get unique dates and their count
                uniques, counts = unique(dates, return_counts=True)
                # Select only the duplicated dates
                duplicates = uniques[counts > 1]
                # Once we had read them already, therefore, we subtract one.
                counts = counts[counts > 1] - 1
                # Get the index of the data we have to duplicate
                idx = asarray(isin(self.dates[isin(self.dates, dates)], duplicates)).nonzero()[0]
                while any(counts > 0):
                    data = append(data, data[idx[counts > 0]], axis=0)
                    counts -= 1

            return data

    def units(self, var: str) -> str:
        """Get unit for the selected variable will return "-" if not unit is set for the selected variable.

        Args:
            var: Get unit for this variable.

        Returns:
            The unit or "-" if not set.
        """
        if var not in self.vars:
            raise ThresherError('Unknown variable %s' % var)

        try:
            with Dataset(self.path) as nc:
                return nc.variables[var].units
        except AttributeError:
            return '-'

    def fill_value(self, var: str) -> Number:
        """Get fill value for the selected variable will return 0 if no fill value is set for the selected variable.

        Args:
            var: Get fill value for this variable.

        Returns:
            The fill value or 0 if not set.
        """
        if var not in self.vars:
            raise ThresherError('Unknown variable %s' % var)

        with Dataset(self.path) as nc:
            return fill_value(nc.variables[var])

    def _cast(self, dates: Union[datetime, List[datetime]]) -> Union[datetime, List[datetime]]:
        """Cast date time objects to the class of the netCDF date."""
        if (isinstance(dates, datetime) and type(dates) == type(self.dates[0])
                or not isinstance(dates, datetime) and type(dates[0]) == type(self.dates[0])):
            return dates

        if isinstance(dates, datetime):
            return self.dates[0].__class__(dates.year, dates.month, dates.day, dates.hour, dates.minute,
                                           dates.microsecond)
        return [
            self.dates[0].__class__(d.year, d.month, d.day, d.hour, d.minute, d.microsecond)
            for d in dates
        ]
