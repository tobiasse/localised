from datetime import timedelta
from numbers import Number
from pathlib import Path
from typing import List
from typing import Tuple
from typing import Union

from cftime import datetime
from click import progressbar
from netCDF4 import Dataset
from netCDF4 import date2num
from netCDF4 import num2date
from numpy import array
from numpy import float64
from numpy import isin
from numpy.typing import NDArray

from localised.thresher.cfg import DIMENSIONS
from localised.thresher.cfg import EXCEED
from localised.thresher.cfg import LAT
from localised.thresher.cfg import LON
from localised.thresher.cfg import TIME
from localised.thresher.errors import ThresherError
from localised.thresher.exceedance import get as get_exceedance


class Aggregation:
    """Helper class for the aggregation of the daily exceedance index to different time scales."""
    daily = 'daily'
    monthly = 'monthly'
    yearly = 'yearly'

    def __init__(self, path: Path) -> None:
        self.path = path

        with Dataset(self.path) as nc:
            for d in DIMENSIONS:
                if d not in nc.dimensions:
                    raise ThresherError('Missing dimension %s' % d)

            time = nc.variables[TIME]
            self.steps = time.steps
            self.units = time.units
            self.calendar = time.calendar
            self.time = num2date(time[:], units=self.units, calendar=self.calendar)

            self.lats = nc.variables[LAT][:]
            self.lons = nc.variables[LON][:]

            exceed = nc.variables[EXCEED]
            self.created = bool(exceed.created)
            self.confidence = exceed.confidence
            self._exceedance = None

            if not self.created:
                self._exceedance = get_exceedance(Path(exceed.exceedance))

    def read(
            self,
            time: datetime,
            lat: Union[Number, List[Number]],
            lon: Union[Number, List[Number]] = None,
    ) -> NDArray[float]:
        """Get exceedance data for the selected time stamp and location.

        Args:
            time: Time stamp of the data to read.
            lat: Latitude coordinates to read.
            lon: Longitude coordinates to read (if not set data for all longitude coordinates is returned)

        Returns:
            The selected data
        """
        with Dataset(self.path) as nc:
            ex = nc.variables[EXCEED]
            if lon is None:
                return ex[self.time == time, isin(self.lats, lat), :]
            return ex[self.time == time, isin(self.lats, lat), isin(self.lons, lon)]

    def run(self) -> 'Aggregation':
        """Aggregate daily exceedance to the selected time scale."""
        if self.created:
            return self

        with progressbar(self.time, label=f'Aggregating {self.steps}') as timestamps:
            for timestamp in timestamps:
                self._write(
                    timestamp,
                    self._exceedance.read(0, self._span(timestamp), self.lats, self.lons).sum(axis=0)
                )

        return self

    def _span(self, time: datetime) -> Tuple[datetime, datetime]:
        """Get start and end time stamp for a selected time with agreement to the selected stepping.

        Args:
            time: The time stamp

        Returns:
            Start and end time stamp
        """
        if self.steps == Aggregation.daily:
            s = time
            e = time + timedelta(days=1)
        elif self.steps == Aggregation.monthly:
            s = time
            if time.month == 12:
                e = time.__class__(time.year + 1, 1, 1)
            else:
                e = time.__class__(time.year, time.month + 1, 1)
        elif self.steps == Aggregation.yearly:
            s = time
            e = time.__class__(time.year + 1, 1, 1)
        else:
            raise ThresherError

        return s, e

    def _write(self, timestamp: datetime, data: NDArray[float]) -> None:
        """Writes job result to netcdf"""
        with Dataset(self.path, mode='a') as nc:
            nc.variables[EXCEED][self.time == timestamp, :, :] = data


class AggregationBootstrapped(Aggregation):
    def run(self) -> 'AggregationBootstrapped':
        years = [timestamp.year for timestamp in self._exceedance.years]

        with progressbar(self.time, label=f'Aggregating bootstrapped {self.steps}') as timestamps:
            for timestamp in timestamps:
                data = list()
                for lay in self._exceedance.layer:
                    if timestamp.year == years[lay // self._exceedance.repetitions]:
                        data.append(self._exceedance.read(lay, self._span(timestamp), self.lats, self.lons).sum(axis=0))

                data = array(data).mean(axis=0)
                if self.steps == Aggregation.daily:
                    data[data <= self.confidence] = 0
                    data[data > self.confidence] = 1

                self._write(timestamp, data)

        return self


def create(
        path: Path,
        exceedance: Path,
        steps: str = Aggregation.yearly,
        confidence: float = .75,
) -> Union[Aggregation, AggregationBootstrapped]:
    exceedance = get_exceedance(exceedance)
    base, units, calendar = exceedance.base, exceedance.units, exceedance.calendar
    lats, lons = exceedance.lats, exceedance.lons

    if exceedance.bootstrapped:
        instance = AggregationBootstrapped
    else:
        instance = Aggregation

    if steps == instance.daily:
        times = exceedance.days
    elif steps == instance.monthly:
        times = exceedance.months
    elif steps == instance.yearly:
        times = exceedance.years
    else:
        raise ThresherError('Unknown time steps')

    with Dataset(path, mode='w') as aggregation:
        aggregation.createDimension(TIME, len(times))
        time = aggregation.createVariable(TIME, float64, (TIME,), fill_value=False)
        time[:] = date2num(times, units=units, calendar=calendar)
        time.base, time.units, time.calendar, time.steps = base, units, calendar, steps

        aggregation.createDimension(LAT, len(lats))
        latitude = aggregation.createVariable(LAT, float64, (LAT,), fill_value=False)
        latitude[:] = lats

        aggregation.createDimension(LON, len(lons))
        longitude = aggregation.createVariable(LON, float64, (LON,), fill_value=False)
        longitude[:] = lons

        exceed = aggregation.createVariable(EXCEED, float64, DIMENSIONS, fill_value=0)
        exceed.created = int(False)
        exceed.exceedance = str(exceedance.path)
        exceed.confidence = confidence
        exceed.percentiles = exceedance.percentiles

    return instance(path)
