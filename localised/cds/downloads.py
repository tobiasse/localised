from typing import List
from typing import Tuple

import cdsapi


def cordex(
        scenario: str,
        temporal: str,
        variables: List[str],
        gcm: str,
        rcm: str,
        ensemble: str,
        start: List[str],
        end: List[str],
        path: str
) -> None:
    """EURO-CORDEX regional climate model data downloader.

    Domain (Europe), spatial resolution (0.11° x 0.11°), and data format (zip) are preselected.

    Args:
        scenario: The experiment hypothesis
        temporal: Temporal resolution of the variable
        variables: The variables that should be downloaded
        gcm: Global climate model
        rcm: Regional climate model
        ensemble: The ensemble member
        start: Starting year of the download
        end: Ending year of the download
        path: Filepath
    """
    c = cdsapi.Client()
    c.retrieve(
        'projections-cordex-domains-single-levels',
        {
            'domain': 'europe',
            'experiment': scenario,
            'horizontal_resolution': '0_11_degree_x_0_11_degree',
            'temporal_resolution': temporal,
            'variable': variables,
            'gcm_model': gcm,
            'rcm_model': rcm,
            'ensemble_member': ensemble,
            'start_year': start,
            'end_year': end,
            'format': 'zip',
        },
        path
    )


# format options
def era5_land(
        path: str,
        format: str,
        variable: str,
        hours: List[str],
        days: List[str],
        months: str,
        years: str,
        area: Tuple[float, float, float, float]
) -> None:
    """ERA5-Land data downloader.

    Data format netCDF is preselected.

    Args:
        path: The path where the downloaded files should be stored.
        variable: The variable to download.
        hours: The hours to download.
        days: The days to download.
        months: The months to download.
        years: The years to download.
        area: The area to download (lat max, lon min, lat min, lon max).
    """
    c = cdsapi.Client()
    c.retrieve(
        'reanalysis-era5-land',
        {
            'format': format,
            'variable': variable,
            'time': hours,
            'day': days,
            'month': months,
            'year': years,
            'area': area
        },
        path
    )


def era5_fire(path: str, variable: str, days: List[str], months: List[str], years: List[str]) -> None:
    """Downloader for fire danger indices from Copernicus Climate Change Service.

    Data format is zip, product type is reanalysis, version is 3.1, and consolidated dataset.

    Args:
        path: The path where the downloaded files should be stored.
        variable: The variable to download.
        days: The days to download.
        months: The month to download.
        years: The years to download.
    """
    c = cdsapi.Client()
    c.retrieve(
        'cems-fire-historical',
        {
            'format': 'zip',
            'product_type': 'reanalysis',
            'variable': variable,
            'version': '3.1',
            'dataset': 'Consolidated dataset',
            'day': days,
            'month': months,
            'year': years,
        },
        path
    )


def fwi(gcm: str, experiment: str, years: List[str], version: str, path: str) -> None:
    """Downloader for projected fire danger indicators from CDS.

    Args:
        gcm: The global climate model.
        experiment: The experiment hypothesis.
        years: The years to download.
        path: Storage location
    """
    c = cdsapi.Client()
    c.retrieve(
        'sis-tourism-fire-danger-indicators',
        {
            'time_aggregation': 'daily_indicators',
            'product_type': 'single_model',
            'variable': 'daily_fire_weather_index',
            'gcm_model': [gcm],
            'experiment': experiment,
            'period': years,
            'format': 'zip',
            'version': version,
        },
        path
    )
