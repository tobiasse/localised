from typing import List

import click

from localised.cds.cfg import cds_ensemble_members
from localised.cds.cfg import cds_experiments
from localised.cds.cfg import cds_gcms
from localised.cds.cfg import cds_rcms
from localised.cds.cfg import cds_temporal_resolution
from localised.cds.cfg import cds_variables
from localised.cds.downloads import cordex
from localised.utils.parse import parse_ranges


@click.command()
@click.option('-s', '--scenario', type=click.Choice(cds_experiments), help='Experiment hypothesis (scenario)')
@click.option('-t', '--temporal', type=click.Choice(cds_temporal_resolution), help='Temporal resolution')
@click.option('-v', '--variables', type=str, help='The variables to download')
@click.option('-g', '--gcm', type=click.Choice(cds_gcms), help='Global climate model')
@click.option('-r', '--rcm', type=click.Choice(cds_rcms), help='Regional climate model')
@click.option('-e', '--ensemble', type=click.Choice(cds_ensemble_members), help='Ensemble member')
@click.option('--start', type=str, callback=parse_ranges(sep=';'), help='Start years')
@click.option('--end', type=str, callback=parse_ranges(sep=';'), help='End years')
@click.argument('path', type=str, required=True)
def download_cordex(
        scenario: str,
        temporal: str,
        variables: str,
        gcm: str,
        rcm: str,
        ensemble: str,
        start: List[str],
        end: List[str],
        path: str
) -> None:
    """Downloader for CORDEX regional climate model data on single levels from climate data store (CDS).
    (https://cds.climate.copernicus.eu/cdsapp#!/dataset/projections-cordex-domains-single-levels?tab=overview)

    Please note the following pre-selections for downloads: domain is Europe, spatial resolution 0.11°x0.11°, and format
    is ZIP. Further, start and end years should be in five year stepping, e.g., for historical --start 1971,1976 --end
    1975,1980. PATH is the full qualified path and name where the downloaded dataset will be stored.
    """
    variables = [v.strip() for v in variables.split(',')]

    for v in variables:
        if v not in cds_variables:
            raise click.BadParameter(f'Unknown variable {v}')

    cordex(scenario, temporal, variables, gcm, rcm, ensemble, start, end, path)
