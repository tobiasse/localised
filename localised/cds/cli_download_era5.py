from os.path import join
from typing import List
from typing import Tuple

from click import Choice
from click import Path
from click import argument
from click import command
from click import option

from localised.cds.cfg import era5_fire_variables
from localised.cds.cfg import era5_variables
from localised.cds.downloads import era5_fire
from localised.cds.downloads import era5_land
from localised.utils.parse import parse_ranges

FORMATS = {
    'grib': 'grib',
    'netcdf.zip': 'zip',
    'netcdf': 'nc',
    'zip': 'zip',
    'tgz': 'tar.gz',
}


@command()
@option(
    '-v',
    '--var',
    type=Choice(era5_variables + era5_fire_variables),
    help='The variable to download.'
)
@option(
    '--hours',
    type=str,
    default='0-23',
    show_default=True,
    callback=parse_ranges(formatting='{:0>2}:00'),
    help=''
)
@option(
    '-d',
    '--days',
    type=str,
    default='1-31',
    callback=parse_ranges(),
    help='The days to download, you may provide ranges (see hours)'
)
@option(
    '-m',
    '--months',
    type=str,
    callback=parse_ranges(),
    help='The months to download, you may provide ranges'
)
@option(
    '-y',
    '--years',
    type=str,
    callback=parse_ranges(),
    help='Obvious or not?'
)
@option(
    '-a',
    '--area',
    type=float,
    nargs=4,
    default=(72., -12., 34., 33.),
    show_default=True,
    help='The area to download.'
)
@option(
    '-f',
    '--format',
    type=Choice(list(FORMATS.keys())),
    callback=lambda ctx, param, value: (value, FORMATS[value]),
    help=''
)
@argument(
    'path',
    type=Path(exists=True, file_okay=False, writable=True),
    required=False
)
def download_era5(
        var: str,
        hours: List[str],
        days: List[str],
        months: List[str],
        years: List[str],
        area: Tuple[float, float, float, float],
        format: Tuple[str, str],
        path: str,
) -> None:
    """Downloader for hourly ERA5-Land data from climate data store (CDS).
    (https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-land?tab=overview)

    Please note the following pre-selections for downloads: format is NetCDF.
    """
    fmt: str
    ext: str
    fmt, ext = format
    template: str = 'era5-land_{var}_{year}_{month}_{mnday}-{mxday}_{mnhour}-{mxhour}.{ext}'  # TODO maybe include area

    mnday: int = min(map(int, days))
    mxday: int = max(map(int, days))

    mnhour: int = min(map(lambda h: int(h.split(':')[0]), hours))
    mxhour: int = max(map(lambda h: int(h.split(':')[0]), hours))

    if not path:
        path = ''

    for year in years:
        for month in months:
            name: str = template.format(
                var=var, year=year, month=month, mnday=mnday, mxday=mxday, mnhour=mnhour, mxhour=mxhour, ext=ext
            )
            filename: str = join(path, name)

            if var in era5_variables:
                era5_land(filename, fmt, var, hours, days, month, year, area)
            else:
                era5_fire(filename, var, days, [month], [year])
