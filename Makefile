install:
	poetry install
.PHONY: install

clean:
	poetry export --output requirements.txt
	pip uninstall -y localised
	pip uninstall -y -r requirements.txt
	pip cache purge
	rm -rf dist/
	rm -rf requirements.txt
.PHONY: clean

build:
	rm -rf dist/
	poetry build
.PHONY: build
