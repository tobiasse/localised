#!/bin/bash

#SBATCH --account=nsp
#SBATCH --job-name=pr

#SBATCH --error=data/err_pr_%j.log
#SBATCH --output=data/out_pr_%j.log
#SBATCH --workdir=/p/tmp/tobiasse/localised

#SBATCH --qos=short

#SBATCH --mem=32000

path=data/raw/

cd $path || exit

roi="eu.shp"
threshold=20     # mm
rescaling="mean" #  [min|max|mean|median|sum]

models=("ncc_noresm1_m-gerics_remo2015" "ncc_noresm1_m-smhi_rca4" "mpi_m_mpi_esm_lr-mpi_csc_remo2009" "mpi_m_mpi_esm_lr-smhi_rca4")
experiment=("historical" "rcp_2_6" "rcp_4_5" "rcp_8_5")
historical=${experiment[0]}

# Regrid and convert to mm/day
for exp in "${experiment[@]}"; do
  for model in "${models[@]}"; do
    cdo \
      -invertlat -remapbil,grid \
      -mulc,1000 -divc,997 -mulc,86400 \
      "${exp}"-mean_precipitation_flux-"${model}".nc \
      "${exp}-daily_precipitation-${model}.nc"
  done
done

# Historical: probability and intensity
for model in "${models[@]}"; do
  # R20mm
  cdo -gtc,"${threshold}" \
    "${historical}-daily_precipitation-${model}.nc" \
    "${historical}-heavy_precipitation-${model}.nc"
  # Probability
  cdo --timestat_date last -divc,12775 -timsum \
    "${historical}-heavy_precipitation-${model}.nc" \
    "${historical}-heavy_precipitation-${model}-probability.nc"
  rescale -v pr --all_touched -a "${rescaling}" \
    "${historical}-heavy_precipitation-${model}-probability.nc" \
    nuts3.shp \
    "${historical}-heavy_precipitation-${model}-probability-${rescaling}.geojson"
  # Intensity (RR20mm)
  cdo --timestat_date first -yearsum \
    "${historical}-heavy_precipitation-${model}.nc" \
    "${historical}-heavy_precipitation-${model}-r20_intensity.nc"
  rescale -v pr --all_touched -a "${rescaling}" \
    "${historical}-heavy_precipitation-${model}-r20_intensity.nc" \
    nuts3.shp \
    "${historical}-heavy_precipitation-${model}-r20_intensity-${rescaling}.geojson"
  # Intensity (RX5day)
  cdo --timestat_date first -yearmax -timselsum,5 \
    "${historical}-daily_precipitation-${model}.nc" \
    "${historical}-heavy_precipitation-${model}-rx5days_intensity.nc"
  rescale -v pr --all_touched -a "${rescaling}" \
    "${historical}-heavy_precipitation-${model}-rx5days_intensity.nc" \
    nuts3.shp \
    "${historical}-heavy_precipitation-${model}-rx5days_intensity-${rescaling}.geojson"
done

# Projections: probability and intensity
timeframe=("2021/2050" "2051/2080" "2081/2100")
for exp in "${experiment[@]:1}"; do
  for model in "${models[@]}"; do
    for frame in "${timeframe[@]}"; do
      t="${frame/\//-}"
      # R20mm
      cdo -gtc,"${threshold}" -selyear,"${frame}" \
        "${exp}-daily_precipitation-${model}.nc" \
        "${exp}-heavy_precipitation-${model}-${t}.nc"
      # Frequency
      cdo --timestat_date first -divc,365 -yearsum \
        "${exp}-heavy_precipitation-${model}-${t}.nc" \
        "${exp}-heavy_precipitation-${model}-frequency-${t}.nc"
      rescale -v pr --all_touched -a "${rescaling}" \
        "${exp}-heavy_precipitation-${model}-frequency-${t}.nc" \
        nuts3.shp \
        "${exp}-heavy_precipitation-${model}-frequency-${t}-${rescaling}.geojson"
      # Intensity (RR20mm)
      cdo --timestat_date first -yearsum \
        "${exp}-heavy_precipitation-${model}-${t}.nc" \
        "${exp}-heavy_precipitation-${model}-r20_intensity-${t}.nc"
      rescale -v pr --all_touched -a "${rescaling}" \
        "${exp}-heavy_precipitation-${model}-r20_intensity-${t}.nc" \
        nuts3.shp \
        "${exp}-heavy_precipitation-${model}-r20_intensity-${t}-${rescaling}.geojson"
      # Intensity (RX5day)
      cdo --timestat_date first -yearmax -timselsum,5 -selyear,"${frame}" \
        "${exp}-daily_precipitation-${model}.nc" \
        "${exp}-heavy_precipitation-${model}-rx5days_intensity-${t}.nc"
      rescale -v pr --all_touched -a "${rescaling}" \
        "${exp}-heavy_precipitation-${model}-rx5days_intensity-${t}.nc" \
        nuts3.shp \
        "${exp}-heavy_precipitation-${model}-rx5days_intensity-${t}-${rescaling}.geojson"
    done
  done
done
