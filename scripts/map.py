# %% Imports and functions
from typing import Callable
from typing import List
from typing import Tuple

import cartopy.feature as cfeature
from cartopy.crs import CRS
from cartopy.crs import PlateCarree
from geopandas import GeoDataFrame
from geopandas import read_file
from matplotlib.cm import ScalarMappable
from matplotlib.colors import BoundaryNorm
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.pyplot import get_cmap
from matplotlib.pyplot import subplots
from shapely.geometry.base import BaseGeometry


def styler(
        data: GeoDataFrame,
        norm: BoundaryNorm,
        cmap: LinearSegmentedColormap,
        column: str,
) -> Callable[[BaseGeometry], dict]:
    def wrapped(geom: BaseGeometry) -> dict:
        val: float = data[data.geometry == geom][column].values[0]
        fc: Tuple[float, float, float, float] = cmap(norm(val))
        style: dict = default_style.copy()
        style.update({'facecolor': fc})
        return style

    return wrapped


# %% Global variables
cm: str = 'YlOrRd'
column: str = 'exposure'
bounds: List[int] = [0, .02, .04, .06, .08, .1, .2, .3, .4, .5, .6, .7, .8, .9, 1.]

crs: CRS = PlateCarree()
extent: Tuple[float, float, float, float] = (-11, 32, 35, 73)

n: int = len(bounds) - 1
norm: BoundaryNorm = BoundaryNorm(boundaries=bounds, ncolors=n)
cmap: LinearSegmentedColormap = get_cmap(cm, lut=n)

default_style: dict = dict(facecolor='white', edgecolor='black', linewidth=.4)

# %% Read data
# fname: str = 'hw_historical_rcps.svg'
# suptitle: str = 'Heat wave exposure'
# gdfs: List[Tuple[GeoDataFrame, str]] = [
#     (read_file('data/proc/hwmid99_historical.geojson'), 'Historical (1861-2005)'),
#     (read_file('data/proc/hwmid99_rcp26.geojson'), 'RCP2.6 (2035-2065)'),
#     (read_file('data/proc/hwmid99_rcp60.geojson'), 'RCP6.0 (2035-2065)'),
#     (read_file('data/proc/hwmid99_rcp85.geojson'), 'RCP8.5 (2035-2065)'),
# ]

# fname: str = 'hw_rcps.svg'
# suptitle: str = 'Heat wave exposure'
# gdfs: List[Tuple[GeoDataFrame, str]] = [
#     (read_file('data/proc/hwmid99_rcp.geojson'), 'RCP mean (2035-2065)'),
#     (read_file('data/proc/hwmid99_rcp26.geojson'), 'RCP2.6 (2035-2065)'),
#     (read_file('data/proc/hwmid99_rcp60.geojson'), 'RCP6.0 (2035-2065)'),
#     (read_file('data/proc/hwmid99_rcp85.geojson'), 'RCP8.5 (2035-2065)'),
# ]

fname: str = 'hw_rcps-historical.svg'
suptitle: str = 'Heat wave (RCP - historical)'
gdfs: List[Tuple[GeoDataFrame, str]] = [
    (read_file('data/proc/hwmid99_historical_rcp_diff_mean.geojson'), 'RCP mean (2035-2065)'),
    (read_file('data/proc/hwmid99_historical_rcp26_diff.geojson'), 'RCP2.6 (2035-2065)'),
    (read_file('data/proc/hwmid99_historical_rcp60_diff.geojson'), 'RCP6.0 (2035-2065)'),
    (read_file('data/proc/hwmid99_historical_rcp85_diff.geojson'), 'RCP8.5 (2035-2065)'),
]

# %% Plot
for gdf, _ in gdfs:
    gdf.columns = [column if c.startswith(column) else c for c in gdf.columns]

fig, axes = subplots(nrows=2, ncols=2, subplot_kw=dict(projection=crs))
fig.suptitle(suptitle)

for ax, (gdf, title) in zip(axes.flatten(), gdfs):
    ax.set_title(title)
    ax.set_extent(extent)

    ax.add_feature(cfeature.OCEAN)
    ax.add_feature(cfeature.LAND)
    ax.add_feature(cfeature.COASTLINE)

    ax.add_geometries(gdf.geometry, crs=crs, styler=styler(gdf, norm, cmap, column))

fig.colorbar(ScalarMappable(norm=norm, cmap=cmap), ax=axes[0, 1])

fig.tight_layout()
# fig.show()
fig.savefig(fname)
