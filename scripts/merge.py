"""
Merge climate data tables
"""
import re
from os import listdir
from pathlib import Path

import geopandas as gpd
import pandas as pd

path = Path('data/proc')
regex = re.compile(r'.+_(\d{4})-\d{2}-\d{2}_\d{2}:\d{2}')

frames = list()
for f in listdir(path):
    fp = path / f
    if fp.suffix not in ['.geojson', '.csv', '.shp']:
        continue
    elif fp.suffix == '.csv':
        frame = pd.read_csv(fp)
    else:
        frame = gpd.read_file(fp)
        frame.drop(labels='geometry', axis=1, inplace=True)

    frame.columns = [regex.match(c).group(1) if regex.match(c) else c for c in frame.columns]

    frame['EXP'] = 'RCP2.6'
    frame['GCM'] = 'MPI-M-MPI-ESM-LR'
    frame['RCM'] = 'SMHI-RCA4-SN'
    frame['ENSEMBLE'] = 'r1i1p1'
    frame['METRIC'] = fp.name
    frame['UNIT'] = ''

    frames.append(frame)
    frame.to_csv(f'/home/tobiasse/Documents/{f}.csv', index=False)

df = pd.concat(frames)
