#!/bin/bash

#SBATCH --account=nsp
#SBATCH --job-name=fr

#SBATCH --error=data/err_fr_%j.log
#SBATCH --output=data/out_fr_%j.log
#SBATCH --workdir=/p/tmp/tobiasse/localised

#SBATCH --qos=short

#SBATCH --mem=32000

path=data/raw/

cd $path || exit

roi="eu.shp"
experiment=("historical" "rcp26" "rcp45" "rcp85")
models=("MPI-M-MPI-ESM-LR" "ICHEC-EC-EARTH")
historical=${experiment[0]}
fwiRanges=(11.2 21.3 21.3 38.0 38.0 50.0)
fwiClasses=("moderate" "moderate" "high" "high" "very-high" "very-high")
suffixes=("_a" "_b")
rescaling="mean" #  [min|max|mean|median|sum]

# Download data
while read -r line; do
  # shellcheck disable=SC2207
  args=($(tr "," "\n" <<<"$line"))
  # 0 exp, 1 gcm, 2 period
  echo "Downloading... ${args[0]}_${args[1]}_${args[2]}.zip"
  fwi -e "${args[0]}" --gcm "${args[1]}" -y "${args[2]}" \
    -p "${args[0]}_${args[1]}_${args[2]}.zip"
  unzip "${args[0]}_${args[1]}_${args[2]}.zip"
  rm "${args[0]}_${args[1]}_${args[2]}.zip"
done <"fwi.csv"

# Merge and regrid
for exp in "${experiment[@]}"; do
  for model in "${models[@]}"; do
    cdo -invertlat -remapbil,grid -mergetime \
      eur11_rca4_"${model}"_"${exp}"_fwi-daily-proj_*.nc "${exp}_fwi_${model}.nc"
  done
done

# Calculate FWI classes
for exp in "${experiment[@]}"; do
  for model in "${models[@]}"; do
    for i in {0..5..2}; do
      cdo -gec,${fwiRanges[i]} \
        "${exp}_fwi_${model}.nc" \
        "${exp}_fwi_${model}_lower-${fwiClasses[i]}.nc"
      cdo -ltc,${fwiRanges[i + 1]} \
        "${exp}_fwi_${model}.nc" \
        "${exp}_fwi_${model}_upper-${fwiClasses[i]}.nc"
      cdo -eqc,2 -enssum \
        "${exp}_fwi_${model}_lower-${fwiClasses[i]}.nc" \
        "${exp}_fwi_${model}_upper-${fwiClasses[i]}.nc" \
        "${exp}_fwi_${model}_${fwiClasses[i]}.nc"
      rm "${exp}_fwi_${model}_lower-${fwiClasses[i]}.nc" "${exp}_fwi_${model}_upper-${fwiClasses[i]}.nc"
    done
  done
done

# Historical: frequency, probability and intensity
for model in "${models[@]}"; do
  for i in {0..5..2}; do
    # Frequency
    cdo --timestat_date first -divc,122 -yearsum -selseason,JJAS \
      "${historical}_fwi_${model}_${fwiClasses[i]}.nc" \
      "${historical}_fire-risk_${model}_${fwiClasses[i]}_frequency.nc"
    rescale -v fwi-daily-proj --all_touched -a "${rescaling}" \
      "${historical}_fire-risk_${model}_${fwiClasses[i]}_frequency.nc" \
      nuts3.shp \
      "${historical}_fire-risk_${model}_${fwiClasses[i]}_frequency_${rescaling}.csv"
    # Probability
    cdo --timestat_date last -divc,4270 -timsum -selseason,JJAS \
      "${historical}_fwi_${model}_${fwiClasses[i]}.nc" \
      "${historical}_fire-risk_${model}_${fwiClasses[i]}_probability.nc"
    rescale -v fwi-daily-proj --all_touched -a "${rescaling}" \
      "${historical}_fire-risk_${model}_${fwiClasses[i]}_probability.nc" \
      nuts3.shp \
      "${historical}_fire-risk_${model}_${fwiClasses[i]}_probability_${rescaling}.csv"
    # Intensity
    cdo --timestat_date first -yearsum -selseason,JJAS \
      "${historical}_fwi_${model}_${fwiClasses[i]}.nc" \
      "${historical}_fire-risk_${model}_${fwiClasses[i]}_intensity.nc"
    rescale -v fwi-daily-proj --all_touched -a "${rescaling}" \
      "${historical}_fire-risk_${model}_${fwiClasses[i]}_intensity.nc" \
      nuts3.shp \
      "${historical}_fire-risk_${model}_${fwiClasses[i]}_intensity_${rescaling}.csv"
  done
done

for i in {0..5..2}; do
  options=""
  for j in {0..1}; do
    python="
from re import compile
from pandas import read_csv

regex = compile(r'^([\w-]+_\d{4})-\d{2}-\d{2}_\d{2}:\d{2}$')
df = read_csv('${historical}_fire-risk_${models[j]}_${fwiClasses[i]}_probability_${rescaling}.csv')
df.columns = [regex.match(c).group(1) if regex.match(c) else c for c in df.columns]
df.to_csv('${historical}_fire-risk_${models[j]}_${fwiClasses[i]}_probability_${rescaling}.csv', index=False)
"
    python <<<"$python"
    classify -c "fwi-daily-proj_2005" --limits 0 0.025 --limits 0.025 0.05 --limits 0.05 1.0 \
      "${historical}_fire-risk_${models[j]}_${fwiClasses[i]}_probability_${rescaling}.csv" \
      "${historical}_fire-risk_${models[j]}_${fwiClasses[i]}_probability_${rescaling}_classified.csv"
    options+="-f "${historical}_fire-risk_${models[j]}_${fwiClasses[i]}_probability_${rescaling}_classified.csv" -s ${suffixes[j]} "
  done
  merge -o NUTS_ID -c fwi-daily-proj_2005 -c fwi-daily-proj_2005_class $options \
    "${historical}_fire-risk_${fwiClasses[i]}_probability_${rescaling}_merged.csv"
  ensemble \
    -c fwi-daily-proj_2005_class_a \
    -c fwi-daily-proj_2005_class_b \
    --default Uncertain \
    --name "probability" \
    "${historical}_fire-risk_${fwiClasses[i]}_probability_${rescaling}_merged.csv" \
    "${historical}_fire-risk_${fwiClasses[i]}_probability_${rescaling}.csv"
done

# Future: frequency and intensity
for exp in "${experiment[@]:1}"; do
  for model in "${models[@]}"; do
    for i in {0..5..2}; do
      # Frequency
      cdo --timestat_date first -divc,122 -yearsum -selseason,JJAS \
        "${exp}_fwi_${model}_${fwiClasses[i]}.nc" \
        "${exp}_fire-risk_${model}_${fwiClasses[i]}_frequency.nc"
      rescale -v fwi-daily-proj --all_touched -a "${rescaling}" \
        "${exp}_fire-risk_${model}_${fwiClasses[i]}_frequency.nc" \
        nuts3.shp \
        "${exp}_fire-risk_${model}_${fwiClasses[i]}_frequency_${rescaling}.csv"
      # Intensity (length)
      cdo --timestat_date first -yearsum -selseason,JJAS \
        "${exp}_fwi_${model}_${fwiClasses[i]}.nc" \
        "${exp}_fire-risk_${model}_${fwiClasses[i]}_intensity-length.nc"
      rescale -v fwi-daily-proj --all_touched -a "${rescaling}" \
        "${exp}_fire-risk_${model}_${fwiClasses[i]}_intensity-length.nc" \
        nuts3.shp \
        "${exp}_fire-risk_${model}_${fwiClasses[i]}_intensity-length_${rescaling}.csv"
    done
  done
done

variable=("frequency" "intensity-length")

for exp in "${experiment[@]:1}"; do
  for var in "${variable[@]}"; do
    for i in {0..5..2}; do
      options=""
      for j in {0..1}; do
        python="
from re import compile
from pandas import read_csv

regex = compile(r'^[\w-]+_(\d{4})-\d{2}-\d{2}_\d{2}:\d{2}$')
df = read_csv('${exp}_fire-risk_${models[j]}_${fwiClasses[i]}_${var}_${rescaling}.csv')
df.columns = [regex.match(c).group(1) if regex.match(c) else c for c in df.columns]
df.to_csv('${exp}_fire-risk_${models[j]}_${fwiClasses[i]}_${var}_${rescaling}.csv', index=False)
"
        python <<<"$python"
        trend -c 24-101 --pettitt \
          "${exp}_fire-risk_${models[j]}_${fwiClasses[i]}_${var}_${rescaling}.csv" \
          "${exp}_fire-risk_${models[j]}_${fwiClasses[i]}_${var}_${rescaling}_trend.csv"
        classify \
          --col cp \
          --limits 2021 2051 --limits 2051 2081 --limits 2081 2101 \
          --classes "Short-term" --classes "Mid-term" --classes "Long-term" \
          "${exp}_fire-risk_${models[j]}_${fwiClasses[i]}_${var}_${rescaling}_trend.csv" \
          "${exp}_fire-risk_${models[j]}_${fwiClasses[i]}_${var}_${rescaling}_trend_timeframe.csv"
        options+="-f ${exp}_fire-risk_${models[j]}_${fwiClasses[i]}_${var}_${rescaling}_trend_timeframe.csv -s ${suffixes[j]} "
      done
      merge -o NUTS_ID -c trend -c p -c cp -c cpp -c cp_class $options \
        "${exp}_fire-risk_${fwiClasses[i]}_${var}_${rescaling}_merged.csv"
      ensemble -c trend_a -c trend_b --default Uncertain --name "$var" \
        "${exp}_fire-risk_${fwiClasses[i]}_${var}_${rescaling}_merged.csv" \
        "${exp}_fire-risk_${fwiClasses[i]}_${var}_${rescaling}_trend.csv"
      ensemble -c cp_class_a -c cp_class_b --default Uncertain --name "timeframe" \
        "${exp}_fire-risk_${fwiClasses[i]}_${var}_${rescaling}_trend.csv" \
        "${exp}_fire-risk_${fwiClasses[i]}_${var}_${rescaling}.csv"
    done
  done
done
