#!/bin/bash

#SBATCH --account=nsp
#SBATCH --job-name=era5

#SBATCH --error=data/err_era5_%j.log
#SBATCH --output=data/out_era5_%j.log
#SBATCH --workdir=/p/tmp/tobiasse/localised

#SBATCH --qos=short

#SBATCH --mem=32000

K=273.15
path=data/raw/

cd $path || exit

for f in era5-land*0-23.nc; do
  cdo -b F32 copy "$f" "${f%.nc}_float.nc"
  cdo --timestat_date first daymin "${f%.nc}_float.nc" "${f%.nc}_min.nc"
  cdo --timestat_date first daymax "${f%.nc}_float.nc" "${f%.nc}_max.nc"
  cdo --timestat_date first daymean "${f%.nc}_float.nc" "${f%.nc}_mean.nc"
  rm "${f%.nc}_float.nc"
done

cdo -subc,"$K" -mergetime era5-land*min.nc era5-land_minimum_2m_temperature_1991-2020.nc
cdo -subc,"$K" -mergetime era5-land*max.nc era5-land_maximum_2m_temperature_1991-2020.nc
cdo -subc,"$K" -mergetime era5-land*mean.nc era5-land_mean_2m_temperature_1991-2020.nc

rm era5-land*min.nc era5-land*max.nc era5-land*mean.nc

# Heat wave
cdo -setmissval,0 -gtc,22 \
  era5-land_minimum_2m_temperature_1991-2020.nc era5-land_minimum_2m_temperature_1991-2020_gtc_22.nc
cdo -setmissval,0 -gtc,30 \
  era5-land_maximum_2m_temperature_1991-2020.nc era5-land_maximum_2m_temperature_1991-2020_gtc_30.nc

cdo eq \
  era5-land_maximum_2m_temperature_1991-2020_gtc_30.nc era5-land_minimum_2m_temperature_1991-2020_gtc_22.nc \
  era5-land_heat-days_1991-2020.nc

rm era5-land_m*_2m_temperature_1991-2020_gtc*.nc

consecutive --var "t2m" era5-land_heat-days_1991-2020.nc era5-land_heatwaves_1991-2020_cum.nc

consecutive --var "t2m" --mode "disagg" era5-land_heat-days_1991-2020.nc era5-land_heatwaves_1991-2020_disagg.nc

cdo --timestat_date first -yearsum -gtc,0 -setmissval,-1 \
  era5-land_heatwaves_1991-2020_cum.nc era5-land_heatwaves_1991-2020_frequency.nc

cdo --timestat_date first -divc,92 -yearsum -selseason,JJA \
  era5-land_heatwaves_1991-2020_disagg.nc era5-land_heatwaves_1991-2020_season.nc

cdo --timestat_date last -divc,2769 -timsum -selseason,JJA \
  era5-land_heatwaves_1991-2020_disagg.nc era5-land_heatwaves_1991-2020_probability.nc

cdo yearmax era5-land_heatwaves_1991-2020_cum.nc era5-land_heatwaves_1991-2020_max.nc

cdo yearsum era5-land_heatwaves_1991-2020_cum.nc era5-land_heatwaves_1991-2020_sum.nc
