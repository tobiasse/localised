# %%
import json
import os
import re
from pathlib import Path

import pandas as pd

# %%
df = pd.DataFrame()
path = 'data/raw/'
regex = re.compile(r'[\w_-]+\[(?P<unit>\w+)\]')
pathways = dict()

for f in os.listdir(path):
    p = path / Path(f)
    if p.is_file() and p.suffix == '.json':
        with open(p) as fd:
            pathways[p.stem] = json.load(fd)

# %%
for pathway, pathway_data in pathways.items():
    for outputs in pathway_data['outputs']:
        id = outputs['id']
        unit = regex.match(id).group('unit')
        time = outputs['timeAxis']
        title = outputs['title']

        rows = list()
        for country, values in outputs['data'].items():
            row = {'pathway': pathway, 'country': country, 'id': id, 'title': title, 'metric': 'absolute', 'unit': unit}
            row.update({year: values for year, values in zip(time, values)})
            rows.append(pd.Series(row))

        frame = pd.DataFrame(rows)
        df = pd.concat([df, frame], ignore_index=True)

# %%
rows = list()
for _, frame in df.groupby(by=['pathway', 'country']):
    tmp = frame.iloc[0]
    row = {'pathway': tmp.pathway, 'country': tmp.country, 'id': 'net_emissions', 'title': 'Net emissions',
           'metric': tmp.metric, 'unit': tmp.unit}
    row.update(frame.sum(numeric_only=True).to_dict())
    rows.append(pd.Series(row))

frame = pd.DataFrame(rows)
df = pd.concat([df, frame], ignore_index=True)


# %%
def relative(row: pd.Series, baseline: int = 1990) -> pd.Series:
    denominator = row[baseline]

    nrow = dict()
    for idx, val in zip(row.index, row):
        if pd.api.types.is_number(val):
            if denominator != 0:
                nrow[idx] = round(val / denominator, 4) * 100
            else:
                nrow[idx] = 0
        else:
            nrow[idx] = val

    nrow['metric'] = 'relative'
    nrow['unit'] = '%'

    return pd.Series(nrow)


percent = df.apply(relative, axis=1)
