#!/bin/bash

#SBATCH --account=nsp
#SBATCH --job-name=cw

#SBATCH --error=data/err_cw_%j.log
#SBATCH --output=data/out_cw_%j.log
#SBATCH --workdir=/p/tmp/tobiasse/localised

#SBATCH --qos=short

#SBATCH --mem=32000

path=data/raw/

cd $path || exit

roi="eu.shp"
startYear=1971
endYear=2000
months="12,1,2,3"
window="5"
percentiles="tasmin<10,tasmax<10"
rescaling="mean" #  [min|max|mean|median|sum]
suffixes=("_a" "_b" "_c" "_d")

models=("ncc_noresm1_m-gerics_remo2015" "ncc_noresm1_m-smhi_rca4" "mpi_m_mpi_esm_lr-mpi_csc_remo2009" "mpi_m_mpi_esm_lr-smhi_rca4")
experiment=("historical" "rcp_2_6" "rcp_4_5" "rcp_8_5")
historical=${experiment[0]}

# Merge tmin and tmax, regrid
for exp in "${experiment[@]}"; do
  for model in "${models[@]}"; do
    cdo -invertlat -remapbil,grid -merge "${exp}"-m*_2m_temperature*"${model}".nc "${exp}-temperature-${model}.nc"
  done
done

# Calculate thresholds
for model in "${models[@]}"; do
  thresher -p $percentiles -y $startYear $endYear -m $months -w $window -r $roi \
    "historical-temperature-${model}.nc" \
    "historical-temperature-${model}-thresholds.nc"
done

# Apply thresholds, daily exceedance index, consecutive days,
for exp in "${experiment[@]}"; do
  for model in "${models[@]}"; do
    apply "${exp}-temperature-${model}.nc" "historical-temperature-${model}-thresholds.nc" \
      "${exp}-temperature-${model}-exceedance.nc"
    daily "${exp}-temperature-${model}-exceedance.nc" "${exp}-temperature-${model}-exceedance-daily.nc"
    setto -m 3 -c 0 -v "exceedance" "${exp}-temperature-${model}-exceedance-daily.nc"
    consecutive -v "exceedance" "${exp}-temperature-${model}-exceedance-daily.nc" \
      "${exp}-coldwaves-${model}.nc"
  done
done

# Historical: probability and intensity
historical=${experiment[0]}
for model in "${models[@]}"; do
  # Frequency
  cdo --timestat_date first -divc,90 -yearsum -selseason,DJF \
    "${historical}-coldwaves-${model}.nc" \
    "${historical}-coldwaves-${model}-frequency.nc"
  rescale -v exceedance --all_touched -a "${rescaling}" \
    "${historical}-coldwaves-${model}-frequency.nc" \
    nuts3.shp \
    "${historical}-coldwaves-${model}-frequency-${rescaling}.csv"
  # Probability
  cdo --timestat_date last -divc,3150 -timsum -selseason,DJF \
    "${historical}-coldwaves-${model}.nc" \
    "${historical}-coldwaves-${model}-probability.nc"
  rescale -v exceedance --all_touched -a "${rescaling}" \
    "${historical}-coldwaves-${model}-probability.nc" \
    nuts3.shp \
    "${historical}-coldwaves-${model}-probability-${rescaling}.csv"
  # Intensity
  cdo --timestat_date first -yearsum -selseason,DJF \
    "${historical}-coldwaves-${model}.nc" \
    "${historical}-coldwaves-${model}-intensity.nc"
  rescale -v exceedance --all_touched -a "${rescaling}" \
    "${historical}-coldwaves-${model}-intensity.nc" \
    nuts3.shp \
    "${historical}-coldwaves-${model}-intensity-${rescaling}.csv"
done

options=""
for i in {0..3}; do
  python="
from re import compile
from pandas import read_csv

regex = compile(r'^([\w-]+_\d{4})-\d{2}-\d{2}_\d{2}:\d{2}$')
df = read_csv('${historical}-coldwaves-${models[i]}-probability-${rescaling}.csv')
df.columns = [regex.match(c).group(1) if regex.match(c) else c for c in df.columns]
df.to_csv('${historical}-coldwaves-${models[i]}-probability-${rescaling}.csv', index=False)
"
  python <<<"$python"
  classify -c "exceedance_2005" --limits 0 0.025 --limits 0.025 0.05 --limits 0.05 1.0 \
    "${historical}-coldwaves-${models[i]}-probability-${rescaling}.csv" \
    "${historical}-coldwaves-${models[i]}-probability-${rescaling}-classified.csv"
  options+="-f ${historical}-coldwaves-${models[i]}-probability-${rescaling}-classified.csv -s ${suffixes[i]} "
done
merge -o NUTS_ID -c exceedance_2005 -c exceedance_2005_class $options \
  "${historical}-coldwaves-probability-${rescaling}-merged.csv"
ensemble \
  -c exceedance_2005_class_a \
  -c exceedance_2005_class_b \
  -c exceedance_2005_class_c \
  -c exceedance_2005_class_d \
  --default Uncertain \
  --name "probability" \
  "${historical}-coldwaves-probability-${rescaling}-merged.csv" \
  "${historical}-coldwaves-probability-${rescaling}.csv"

# Future: frequency and intensity
for exp in "${experiment[@]:1}"; do
  for model in "${models[@]}"; do
    # Frequency
    cdo --timestat_date first -divc,90 -yearsum -selseason,DJF \
      "${exp}-coldwaves-${model}.nc" \
      "${exp}-coldwaves-${model}-frequency.nc"
    rescale -v exceedance --all_touched -a "${rescaling}" \
      "${exp}-coldwaves-${model}-frequency.nc" \
      nuts3.shp \
      "${exp}-coldwaves-${model}-frequency-${rescaling}.csv"
    # Intensity (max)
    cdo --timestat_date first -yearmax -selseason,DJF \
      "${exp}-coldwaves-${model}.nc" \
      "${exp}-coldwaves-${model}-intensity_max.nc"
    rescale -v exceedance --all_touched -a "${rescaling}" \
      "${exp}-coldwaves-${model}-intensity_max.nc" \
      nuts3.shp \
      "${exp}-coldwaves-${model}-intensity_max-${rescaling}.csv"
    # Intensity (length)
    cdo --timestat_date first -yearsum -selseason,DJF \
      "${exp}-coldwaves-${model}.nc" \
      "${exp}-coldwaves-${model}-intensity_length.nc"
    rescale -v exceedance --all_touched -a "${rescaling}" \
      "${exp}-coldwaves-${model}-intensity_length.nc" \
      nuts3.shp \
      "${exp}-coldwaves-${model}-intensity_length-${rescaling}.csv"
  done
done

variable=("frequency" "intensity_max" "intensity_length")

for exp in "${experiment[@]:1}"; do
  for var in "${variable[@]}"; do
    options=""
    for i in {0..3}; do
      python="
from re import compile
from pandas import read_csv

regex = compile(r'^[\w-]+_(\d{4})-\d{2}-\d{2}_\d{2}:\d{2}$')
df = read_csv('${exp}-coldwaves-${models[i]}-${var}-${rescaling}.csv')
df.columns = [regex.match(c).group(1) if regex.match(c) else c for c in df.columns]
df.to_csv('${exp}-coldwaves-${models[i]}-${var}-${rescaling}.csv', index=False)
"
      python <<<"$python"
      trend -c 24-103 --pettitt \
        "${exp}-coldwaves-${models[i]}-${var}-${rescaling}.csv" \
        "${exp}-coldwaves-${models[i]}-${var}-${rescaling}-trend.csv"
      classify \
        --col cp \
        --limits 2021 2051 --limits 2051 2081 --limits 2081 2101 \
        --classes "Short-term" --classes "Mid-term" --classes "Long-term" \
        "${exp}-coldwaves-${models[i]}-${var}-${rescaling}-trend.csv" \
        "${exp}-coldwaves-${models[i]}-${var}-${rescaling}-trend-timeframe.csv"
      options+="-f ${exp}-coldwaves-${models[i]}-${var}-${rescaling}-trend-timeframe.csv -s ${suffixes[i]} "
    done
    merge -o NUTS_ID -c trend -c p -c cp -c cpp -c cp_class $options \
      "${exp}-coldwaves-${var}-${rescaling}-merged.csv"
    ensemble -c trend_a -c trend_b -c trend_c -c trend_d --default Uncertain --name "$var" \
      "${exp}-coldwaves-${var}-${rescaling}-merged.csv" \
      "${exp}-coldwaves-${var}-${rescaling}-trend.csv"
    ensemble -c cp_class_a -c cp_class_b -c cp_class_c -c cp_class_d --default Uncertain --name "timeframe" \
      "${exp}-coldwaves-${var}-${rescaling}-trend.csv" \
      "${exp}-coldwaves-${var}-${rescaling}.csv"
  done
done
