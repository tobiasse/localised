#!/bin/bash

#SBATCH --account=nsp
#SBATCH --job-name=era5

#SBATCH --error=data/err_era5_%j.log
#SBATCH --output=data/out_era5_%j.log
#SBATCH --workdir=/p/tmp/tobiasse/localised

#SBATCH --qos=short

#SBATCH --mem=32000

K=273.15
path=data/raw/

cd $path || exit

for f in rcp_*-m*_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4.nc; do
  if [[ "$f" == *"minimum"* ]]; then
    var="tasmin"
  elif [[ "$f" == *"maximum"* ]]; then
    var="tasmax"
  else
    continue
  fi
  cdo -invertlat -remapbil,grid -chname,"$var",t2m -subc,"$K" -selyear,2021/2100 "$f" "${f%.nc}_2021-2100.nc"
done

for rcp in "rcp_2_6" "rcp_4_5" "rcp_8_5"; do
  cdo -setmissval,0 -gtc,22 \
    "${rcp}-minimum_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_2021-2100.nc" \
    "${rcp}-minimum_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_2021-2100_gtc_22.nc"

  cdo -setmissval,0 -gtc,30 \
    "${rcp}-maximum_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_2021-2100.nc" \
    "${rcp}-maximum_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_2021-2100_gtc_30.nc"

  cdo eq \
    "${rcp}-minimum_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_2021-2100_gtc_22.nc" \
    "${rcp}-maximum_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_2021-2100_gtc_30.nc" \
    "${rcp}-heat-days_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_2021-2100.nc"

  consecutive --var "t2m" \
    "${rcp}-heat-days_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_2021-2100.nc" \
    "${rcp}-heatwaves_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_2021-2100_cum.nc"

  consecutive --var "t2m" --mode "disagg" \
    "${rcp}-heat-days_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_2021-2100.nc" \
    "${rcp}-heatwaves_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_2021-2100_disagg.nc"

  for time in "2021/2050" "2051/2080" "2081/2100"; do
    t="${time/\//-}"
    days="2769"
    if [[ "$time" == "2081/2100" ]]; then
      days="1840"
    fi

    cdo selyear,"$time" \
      "${rcp}-heatwaves_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_2021-2100_cum.nc" \
      "${rcp}-heatwaves_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_${t}_cum.nc"

    cdo selyear,"$time" \
      "${rcp}-heatwaves_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_2021-2100_disagg.nc" \
      "${rcp}-heatwaves_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_${t}_disagg.nc"

    cdo --timestat_date first -yearsum -gtc,0 -setmissval,-1 \
      "${rcp}-heatwaves_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_${t}_cum.nc" \
      "${rcp}-heatwaves_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_${t}_frequency.nc"

    cdo --timestat_date first -divc,92 -yearsum -selseason,JJA \
      "${rcp}-heatwaves_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_${t}_disagg.nc" \
      "${rcp}-heatwaves_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_${t}_season.nc"

    cdo --timestat_date last -divc,"$days" -timsum -selseason,JJA \
      "${rcp}-heatwaves_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_${t}_disagg.nc" \
      "${rcp}-heatwaves_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_${t}_probability.nc"

    cdo yearmax \
      "${rcp}-heatwaves_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_${t}_cum.nc" \
      "${rcp}-heatwaves_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_${t}_max.nc"

    cdo yearsum \
      "${rcp}-heatwaves_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_${t}_cum.nc" \
      "${rcp}-heatwaves_2m_temperature_in_the_last_24_hours-mpi_m_mpi_esm_lr-smhi_rca4_${t}_sum.nc"
  done
done
